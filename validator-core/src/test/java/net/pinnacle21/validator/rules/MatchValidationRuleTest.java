/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static net.pinnacle21.validator.rules.ValidationRuleTestHelper.*;

/**
 *
 * @author Tim Stone
 */
public class MatchValidationRuleTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);
    private final ValidationSession session = ValidationSession.create(this.options, new CancellationToken(),
            this.factory, null);

    @Test
    public void CanMatchPairs() throws Exception {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("PairedVariable")).thenReturn(true);
        when(definition.getProperty("PairedVariable")).thenReturn("YY");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A:B,C:D");

        ValidationRule rule = new MatchValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = createRecords(new String[] { "XX", "YY" },
            new DataEntry[] { this.factory.create("A"), this.factory.create("B") },
            new DataEntry[] { this.factory.create("C"), this.factory.create("D") },
            new DataEntry[] { this.factory.create("C"), this.factory.create("B") },
            new DataEntry[] { this.factory.create("C"), this.factory.create("R") }
        );

        Boolean[] expected = new Boolean[] {
            true, true, false, false
        };
        Boolean[] results = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            results[index++] = rule.validate(record);
        }

        assertArrayEquals(expected, results);
    }
}
