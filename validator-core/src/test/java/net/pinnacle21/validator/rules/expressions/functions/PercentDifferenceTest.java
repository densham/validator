/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Tim Stone
 */
public class PercentDifferenceTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void SubtractionIsEasy() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create(2), this.factory.create(1) }
        );
        Function function = new Difference("PCTDIFF", this.factory, names);

        assertEquals(this.factory.create(1), function.compute(records.get(0)));
    }
}
