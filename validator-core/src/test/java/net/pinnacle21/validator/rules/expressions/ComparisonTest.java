/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions;

import static org.junit.Assert.*;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

import java.util.List;

/**
 * @author Tim Stone
 */
public class ComparisonTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    /**
     * Assertion: A fuzzy comparison against RHS value of 0 should not return false
     *     when the LHS value is also 0
     */
    @Test
    public void FuzzyEqualityZeroWorks() {
        Evaluable comparison = Comparison.createComparison("A %= 0", this.options, this.factory);
        List<DataRecord> records = ValidationRuleTestHelper.createRecords("A", this.factory.create(0.0));

        assertTrue(comparison.evaluate(records.get(0)));
    }
}
