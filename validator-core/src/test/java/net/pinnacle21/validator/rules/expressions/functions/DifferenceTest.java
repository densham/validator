/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Tim Stone
 */
public class DifferenceTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void SubtractionIsEasy() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create(6), this.factory.create(4) }
        );
        Function function = new Difference("DIFF", this.factory, names);

        assertEquals(this.factory.create(2), function.compute(records.get(0)));
    }

    @Test
    public void FloatingPointSubtractionIsHard() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create(28.1), this.factory.create(19.7) },
            new DataEntry[] { this.factory.create(28.1), this.factory.create(34.5) },
            new DataEntry[] { this.factory.create(28.1), this.factory.create(13.1) }
        );
        Function function = new Difference("DIFF", this.factory, names);

        assertEquals(this.factory.create(8.4), function.compute(records.get(0)));
        assertEquals(this.factory.create(-6.4), function.compute(records.get(1)));
        assertEquals(this.factory.create(15.0), function.compute(records.get(2)));
    }
}
