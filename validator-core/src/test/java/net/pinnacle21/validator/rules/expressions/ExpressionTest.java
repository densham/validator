/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class ExpressionTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void IllegalGroupReference() {
        this.expression("AEORRES != '' @and AEORRES @re '[-+]?[0-9]*\\\\.?[0-9]+' @and AETEST @re '^((?!( Ratio)).)*'");
    }

    @Test
    public void RepeatRegularExpressions() {
        DataRecord record;
        Expression expression = this.expression("XY @re '[A-Z]+'");

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("ABC"));

        assertTrue(expression.evaluate(record));

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("123"));

        assertFalse(expression.evaluate(record));

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("LMN"));

        assertTrue(expression.evaluate(record));
    }

    /**
     * Assertion: Backslashes ("|") shouldn't be replaced during the constant
     *     resubstitute process.
     */
    @Test
    public void SlashesInLiteralsShouldRemainThere() {
        DataRecord record;
        Expression expression;

        expression = this.expression("XY @re '[-+]?[0-9]*\\.?[0-9]+'");

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("<1"));

        assertFalse(expression.evaluate(record));

        expression = this.expression("XY == '\\a'");

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("\\a"));

        assertTrue(expression.evaluate(record));

        expression = this.expression("XY == '\\''");

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(this.factory.create("'"));

        assertTrue(expression.evaluate(record));
    }

    /**
     * Assertion: Internal variable names of the form VAL:VARIABLE should be considered
     *     valid in an expression.
     */
    @Test
    public void InternalVariableNamesPermitted() {
        Expression expression = this.expression("VAL:DATASET == 'FAEX'");

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("VAL:DATASET",
            this.factory.create("FAEX"),
            this.factory.create("XX")
        );

        Boolean[] expected = new Boolean[] { true, false };
        Boolean[] actual = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            actual[index++] = expression.evaluate(record);
        }

        assertArrayEquals(expected, actual);
    }

    @Test
    public void LiteralsAreAllowedAsFunctionParams() {
        Expression expression = this.expression(
            "(LBTESTCD == 'AST' @or LBTESTCD =='ALT') @and LBORNRHI @lt :DIV(LBORRES,3)"
        );

        Set<String> expected = new HashSet<>(Arrays.asList(
            "LBTESTCD", "LBORNRHI", "LBORRES"
        ));

        assertEquals(expected, expression.getVariables());
    }

    @Test
    public void EscapedSingleQuotesInLiteralsAreActuallyEscaped() {
        Expression expression = this.expression("VAR == 'STUFF in (\\'ABC\\', \\'DEF\\')'");

        DataRecord record = mock(DataRecord.class);
        when(record.getValue("VAR")).thenReturn(this.factory.create("STUFF in ('ABC', 'DEF')"));

        assertTrue(expression.evaluate(record));
    }

    private Expression expression(String expression) {
        return Expression.createFrom(expression, this.options, this.factory);
    }
}
