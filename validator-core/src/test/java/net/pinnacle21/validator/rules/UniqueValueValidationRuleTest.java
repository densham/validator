/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static net.pinnacle21.validator.rules.ValidationRuleTestHelper.MOCK_SCOPE;

/**
 * @author Tim Stone
 */
public class UniqueValueValidationRuleTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);
    private final ValidationSession session = ValidationSession.create(this.options, new CancellationToken(),
            this.factory, null);

    /**
     * Assertion: If all values for a non-matching Unique rule are unique, the rule
     *     should pass.
     */
    @Test
    public void SingleColumnUniqueSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("No");

        ValidationRule rule = new UniqueValueValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        boolean failure = false;

        for (DataRecord record : records) {
            if (!rule.validate(record)) {
                failure = true;
            }
        }

        assertFalse(failure);
    }

    /**
     * Assertion: If all values for a non-matching Unique rule aren't unique, the rule
     *     should fail on subsequent duplicates.
     */
    @Test
    public void SingleColumnUniqueFailure() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("No");

        ValidationRule rule = new UniqueValueValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("B"),
            this.factory.create("C"),
            this.factory.create("A")
        );

        // Boolean instead of boolean because junit lacks an assertArrayEquals(b[], b[])
        // for whatever reason
        Boolean[] expected = new Boolean[] {
            false, false, true, false, true
        };
        Boolean[] failures = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            failures[index++] = !rule.validate(record);
        }

        assertArrayEquals(expected, failures);
    }

    /**
     * Assertion: If all values for a matching Unique rule are the same, the rule
     *     should pass.
     */
    @Test
    public void SingleColumnMatchingSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("Yes");

        ValidationRule rule = new UniqueValueValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("A"),
            this.factory.create("A")
        );

        boolean failure = false;

        for (DataRecord record : records) {
            if (!rule.validate(record)) {
                failure = true;
            }
        }

        assertFalse(failure);
    }

    /**
     * Assertion: If all values for a matching Unique rule are not the same, the rule
     *     should fail on subsequent non-matching values.
     */
    @Test
    public void SingleColumnMatchingFailure() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("Yes");

        ValidationRule rule = new UniqueValueValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("A"),
            this.factory.create("A"),
            this.factory.create("C")
        );

        Boolean[] expected = new Boolean[] {
            false, true, false, false, true
        };
        Boolean[] failures = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            failures[index++] = !rule.validate(record);
        }

        assertArrayEquals(expected, failures);
    }
}
