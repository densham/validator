/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import static org.junit.Assert.*;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

/**
 * @author Tim Stone
 */
public class FunctionsTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    /**
     * Assertion: Functions can be created post-refactor.
     */
    @Test
    public void CanCreateFunction() {
        assertNotNull(Functions.create(":DY(AA, BB)", this.factory));
    }

    @Test
    public void FunctionLiteralsAreNotVariables() {
        Function function = Functions.create(":DIV(LBORRES,3)", this.factory);

        DataRecord record = ValidationRuleTestHelper.createRecords("LBORRES", this.factory.create(9)).get(0);

        assertEquals(this.factory.create(3), function.compute(record));
    }
}
