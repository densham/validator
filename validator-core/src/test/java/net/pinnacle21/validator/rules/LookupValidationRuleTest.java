/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.*;
import org.junit.Test;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static net.pinnacle21.validator.rules.ValidationRuleTestHelper.MOCK_SCOPE;

/**
 *
 * @author Tim Stone
 */
public class LookupValidationRuleTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);
    private final LookupProvider lookupProvider = new ConcurrentLookupProvider(this.factory,
            new SourceProvider(this.factory), Collections.emptySet());
    private final ValidationSession session = ValidationSession.create(this.options, new CancellationToken(),
            this.factory, new LookupProviderFactory(this.lookupProvider));

    /**
     * Ensures that a LookupValidationRule can perform checks using an external provider
     */
    @Test
    public void ExternalProviderLookup() throws Exception {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX == XX");
        when(definition.hasProperty("From")).thenReturn(true);
        when(definition.getProperty("From")).thenReturn("XX");
        when(definition.hasProperty("Provider")).thenReturn(true);
        when(definition.getProperty("Provider")).thenReturn(
            "net.pinnacle21.validator.rules.LookupValidationRuleTest$LookupProviderImpl"
        );

        LookupValidationRule rule = new LookupValidationRule(definition, this.session, MOCK_SCOPE);
        DataRecord record = new DataRecordImpl(null, null, ImmutableMap.of("XX", this.factory.create("Y")));

        assertTrue(rule.validate(record));
    }

    @Test
    public void CaseInsensitiveClauseWorks() throws Exception {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Search")).thenReturn(true);
        when(definition.getProperty("Search")).thenReturn(
            "CDISCSubmissionValue @eqic [XX]"
        );
        when(definition.hasProperty("From")).thenReturn(true);
        when(definition.getProperty("From")).thenReturn(
            "file://" + this.pathTo("/samples/rules/LookupValidationRule/ct.txt") + "?type=tab"
        );
        when(definition.hasProperty("Where")).thenReturn(true);
        when(definition.getProperty("Where")).thenReturn("CodeListCode == 'C65047'");

        ValidationRule rule;
        List<DataRecord> records;

        rule = new LookupValidationRule(definition, this.session, MOCK_SCOPE);
        records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("aniso")
        );

        assertTrue(rule.validate(records.get(0)));

        when(definition.getProperty("Where")).thenReturn("CodeListCode == 'C67154'");

        rule = new LookupValidationRule(definition, this.session, MOCK_SCOPE);
        records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("ANISOCYTES"),
            this.factory.create("Anisocytes")
        );

        for (DataRecord record : records) {
            assertTrue(rule.validate(record));
        }
    }

    /**
     * Concrete mock implementation of a LookupProvider
     *
     * @author Tim Stone
     */
    public static class LookupProviderImpl implements LookupProvider {
        @SuppressWarnings("unchecked")
        public Lookup get(String sourceName, Set<String> variables) {
            Lookup lookup = mock(Lookup.class);

            when(lookup.seek(anyList(), anyList(), anyBoolean())).thenReturn(true);

            return lookup;
        }
        public void request(String sourceName, Set<String> variables) {}
        public boolean verifyExists(String sourceName) {
            return true;
        }
        public boolean verifyExists(String sourceName, Set<String> variables) {
            return true;
        }
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
