/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import static org.junit.Assert.*;

import org.junit.Test;
import net.pinnacle21.validator.settings.Definition;

/**
 * @author Tim Stone
 */
public class DefinitionTest {
    /**
     *
     */
    @Test
    public void SelectiveCopy() {
        Definition source = new Definition(Definition.Target.Domain, "domain");
        Definition destination = new Definition(Definition.Target.Domain, "domain");

        source.setProperty("a", "source");
        source.setProperty("b", "source");
        destination.setProperty("a", "destination");
        destination.setProperty("b", "destination");

        Definition.copyTo(source, destination, "a");

        assertEquals("source", destination.getProperty("a"));
        assertEquals("destination", destination.getProperty("b"));
    }
}
