/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.*;

/**
 * @author Tim Stone
 */
public class ValueListTest {
    @Test
    public void invalidCheckInvalidatesClauseExpression() {
        ValueList valueList = new ValueList("ABC");
        ValueList.Clause clause = valueList.addClause("XYZ", "Yes");

        clause.addCheck("IT.VARA", "EQ", Collections.singleton("1"));
        clause.addCheck("IT.VARB", "FAKE", Collections.singleton("X"));

        Map<String, Definition> variables = new HashMap<>();

        variables.put("IT.VARA", new Definition(Definition.Target.Variable, "VARA"));
        variables.put("IT.VARB", new Definition(Definition.Target.Variable, "VARB"));

        assertNull(clause.getExpression(variables));
    }

    @Test
    public void validChecksStillCreateClauseExpression() {
        ValueList valueList = new ValueList("ABC");
        ValueList.Clause clause = valueList.addClause("XYZ", "Yes");

        clause.addCheck("IT.VARA", "EQ", Collections.singleton("1"));
        clause.addCheck("IT.VARB", "IN", new LinkedHashSet<>(Arrays.asList("X", "Y")));

        Map<String, Definition> variables = new HashMap<>();

        variables.put("IT.VARA", new Definition(Definition.Target.Variable, "VARA"));
        variables.put("IT.VARB", new Definition(Definition.Target.Variable, "VARB"));

        assertEquals("(VARA == '1') @and (VARB == 'X' @or VARB == 'Y')", clause.getExpression(variables));
    }

    @Test
    public void noChecksInvalidatesClauseExpression() {
        ValueList valueList = new ValueList("ABC");
        ValueList.Clause clause = valueList.addClause("XYZ", "Yes");

        assertNull(clause.getExpression(Collections.emptyMap()));
    }
}
