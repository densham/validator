/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import net.pinnacle21.validator.api.model.Diagnostic;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Tim Stone
 */
public class RuleDefinitionTest {
    @Test
    public void PriorityMatchingReplacesSeverityCorrectly() {
        RuleDefinition base = new RuleDefinition("XX", "RULE", Diagnostic.Type.Warning)
            .withConditionalSeverity(new RuleDefinition.ConditionalSeverity("AE", "", Diagnostic.Type.Error))
            .withConditionalSeverity(new RuleDefinition.ConditionalSeverity("AE", "ABC", Diagnostic.Type.Notice));

        RuleDefinition definition;

        definition = base.withSeverityFor("AE");

        assertEquals(Diagnostic.Type.Error, definition.getType());

        definition = base.withContext("ABC")
            .withSeverityFor("AE");

        assertEquals(Diagnostic.Type.Notice, definition.getType());
    }
}
