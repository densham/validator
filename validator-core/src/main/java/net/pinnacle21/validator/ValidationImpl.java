/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.pinnacle21.validator.api.BaseValidation;
import net.pinnacle21.validator.api.events.RuleListener;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.*;
import net.pinnacle21.validator.data.*;
import net.pinnacle21.validator.engine.BlockValidator;
import net.pinnacle21.validator.engine.EngineEntity;
import net.pinnacle21.validator.engine.ValidationEngine;
import net.pinnacle21.validator.report.DiagnosticImpl;
import net.pinnacle21.validator.report.RuleMetadata;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.report.WritableRuleMetricsImpl;
import net.pinnacle21.validator.settings.Configuration;
import net.pinnacle21.validator.settings.ConfigurationManager;
import net.pinnacle21.validator.settings.ConfigurationParser;
import net.pinnacle21.validator.settings.XmlConfigurationParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public final class ValidationImpl extends BaseValidation<ValidationImpl> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationImpl.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final List<SourceOptions> sources;
    private final ConfigOptions config;
    private final ValidationOptions options;
    private final ExecutorService sharedExecutor;

    ValidationImpl(List<SourceOptions> sources, ConfigOptions config, ValidationOptions options,
            ExecutorService sharedExecutor) {
        this.sources = sources;
        this.config = config;
        this.options = options != null
                ? options
                : ValidationOptions.builder().build();
        this.sharedExecutor = sharedExecutor;
    }

    @Override
    public CompletionStage<ValidationResult> executeAsync(CancellationToken cancellationToken) {
        ExecutorService executor = this.createExecutorService();
        DataEntryFactory factory = new DataEntryFactory(this.options);
        WritableRuleMetrics metrics = new WritableRuleMetricsImpl();
        SourceProvider sourceProvider = new SourceProvider(factory);
        LookupProvider lookupProvider = new ConcurrentLookupProvider(factory, sourceProvider, this.eventListeners);

        ValidationSession session = ValidationSession.create(this.options, cancellationToken, factory,
                new LookupProviderFactory(lookupProvider));
        ValidationResult.Builder resultBuilder = ValidationResult.builder()
                .withMetrics(metrics);

        SourceDetails global = new InternalEntityDetails(SourceDetails.Reference.Data, ImmutableMap.of(
            SourceDetails.Property.Name, "GLOBAL",
            SourceDetails.Property.Location, "",
            SourceDetails.Property.Label, "Global Metadata"
        ), null);
        DefineCheck defineCheck = new DefineCheck(metrics, global);

        CompletableFuture<Void> testSources = CompletableFuture
                .runAsync(() -> sourceProvider.add(this.sources));
        CompletableFuture<ConfigurationManager> parseConfiguration = CompletableFuture
                .supplyAsync(() -> this.createManager(session, metrics, defineCheck));

        Set<SourceDetails> sourceDetails = Collections.synchronizedSet(new LinkedHashSet<>(Collections.singleton(
            global
        )));

        return parseConfiguration.thenCombine(testSources, (manager, v) -> {
            ValidationEngine engine = new BlockValidator(session, this.diagnosticListeners, this.eventListeners,
                    executor, manager.prepare("GLOBAL", null, true));
            Set<String> sourceNames = sourceProvider.getSourceNames();

            // TODO: We should collect these in a saner way; temporary hack for now
            for (String sourceName : sourceNames) {
                DataSource source = sourceProvider.getSource(sourceName);

                sourceDetails.add(source.getDetails());
            }
            // end --^

            for (String sourceName : sourceNames) {
                if (session.shouldCancel()) {
                    return false;
                }

                DataSource source = sourceProvider.getSource(sourceName);
                Set<String> variables = Collections.emptySet();

                try {
                    variables = source.getVariables();
                } catch (InvalidDataException ignore) {}

                Configuration configuration = manager.prepare(sourceName, variables, true);

                engine.prepare(new EngineEntity(source, configuration));
            }

            DISPATCHER.dispatchTo(this.ruleListeners, d -> d::complete);

            defineCheck.validate(this.config);

            return engine.validate();
        }).handle((wasSuccessful, throwable) -> {
            if (executor != this.sharedExecutor) {
                executor.shutdownNow();
            }

            return resultBuilder
                    .withCompletedSuccessfully(wasSuccessful != null && wasSuccessful)
                    .withSources(sourceDetails)
                    .withCause(throwable)
                    .build();
        });
    }

    @Override
    protected final ValidationImpl recreate() {
        return new ValidationImpl(this.sources, this.config, this.options, this.sharedExecutor);
    }

    private ConfigurationManager createManager(ValidationSession session, WritableRuleMetrics metrics,
            RuleListener defineListener) {
        File define = null;
        String definePath = this.config.getDefine();

        if (definePath != null && definePath.length() > 0) {
            define = new File(definePath);
        }

        Collection<File> configFiles = this.config.getConfigs()
                .stream()
                .map(File::new)
                .collect(Collectors.toList());
        Map<String, String> properties = new HashMap<>();

        for (String property : this.config.getProperties()) {
            properties.put(property, this.config.getProperty(property));
        }

        Set<RuleListener> ruleListeners = ImmutableSet.<RuleListener>builder()
                .addAll(this.ruleListeners)
                .add(defineListener)
                .build();

        ConfigurationParser parser = new XmlConfigurationParser(configFiles, define, properties);
        ConfigurationManager manager = new ConfigurationManager(session, metrics, ruleListeners, properties);

        parser.parse(manager);

        return manager;
    }

    private ExecutorService createExecutorService() {
        LOGGER.debug("Validation will be invoked using {} bytes of RAM", Runtime.getRuntime().maxMemory());

        if (this.sharedExecutor != null) {
            LOGGER.debug("Validation will be invoked using externally-provided thread pool");

            return this.sharedExecutor;
        }

        int processors = Runtime.getRuntime().availableProcessors() - 1;
        int threads = 0;

        if (this.options.hasProperty("Engine.ThreadCount")) {
            String setting = this.options.getProperty("Engine.ThreadCount");

            if (setting.equalsIgnoreCase("auto")) {
                threads = processors;
            } else {
                try {
                    threads = Integer.parseInt(setting);
                } catch (NumberFormatException ignore) {
                }
            }
        }

        threads = Math.max(Math.min(threads, processors), 1);

        LOGGER.debug("Validation will be invoked using internal {} count thread pool", threads);

        return Executors.newFixedThreadPool(threads);
    }

    private class DefineCheck implements RuleListener {
        private static final String DEFINE_PRESENCE_ID = "DD0101";

        private final WritableRuleMetrics metrics;
        private final SourceDetails global;
        private RuleMetadata metadata;

        DefineCheck(WritableRuleMetrics metrics, SourceDetails global) {
            this.metrics = metrics;
            this.global = global;
        }

        void validate(ConfigOptions options) {
            if (this.metadata == null) {
                return;
            }

            WritableRuleMetrics.RuleMetric ruleMetric = this.metrics.getRule(
                this.global.getString(SourceDetails.Property.Name),
                SourceDetails.Reference.Data,
                DEFINE_PRESENCE_ID,
                null,
                this.metadata.getMessage(),
                this.metadata.getType()
            );

            ruleMetric.start();

            boolean isDefineAbsent = StringUtils.isBlank(options.getDefine())
                    || !Files.exists(Paths.get(options.getDefine()));

            if (isDefineAbsent) {
                DISPATCHER.dispatchTo(diagnosticListeners, d -> d, Collections.singletonList(new DiagnosticImpl(
                    metadata, this.global, null, Collections.emptyMap()
                )));
            }

            ruleMetric.stop(true, isDefineAbsent);
        }

        @Override
        public void acceptTemplate(RuleTemplate template) {
            if ("DD0101".equals(template.getId())) {
                this.metadata = new RuleMetadata(
                    this.global.getString(SourceDetails.Property.Name),
                    template.getId(),
                    template.getPublisherId(),
                    null,
                    template.getCategory(),
                    template.getMessage(),
                    template.getDescription(),
                    template.getType()
                );

                DISPATCHER.dispatchTo(ruleListeners, d -> d::acceptInstance, this.metadata);
            }
        }
    }
}
