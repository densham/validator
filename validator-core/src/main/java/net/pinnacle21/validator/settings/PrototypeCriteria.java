/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

class PrototypeCriteria {
    private final String datasetName;
    private final List<String> variables;

    PrototypeCriteria(String datasetName, String[] keyVariables) {
        this.datasetName = datasetName;
        this.variables = Arrays.asList(keyVariables);
    }

    String getDatasetName() {
        return this.datasetName;
    }

    boolean hasDatasetName() {
        return StringUtils.isNotEmpty(this.datasetName);
    }

    List<String> getVariables() {
        return this.variables;
    }

    boolean hasVariables() {
        return !this.variables.isEmpty();
    }

    boolean isFallbackCriteria() {
        return !this.hasDatasetName()
               && this.variables.size() == 1
               && this.variables.get(0).equals("*");
    }
}
