/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import net.pinnacle21.validator.api.events.RuleListener;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.report.RuleMetadata;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.rules.*;
import net.pinnacle21.validator.util.Helpers;
import net.pinnacle21.validator.util.KeyMap;
import net.pinnacle21.validator.ValidationSession;
import net.pinnacle21.validator.settings.ConfigurationException.Type;
import net.pinnacle21.validator.settings.Definition.Target;
import net.pinnacle21.validator.settings.MagicVariableParser.MagicProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 * @author Tim Stone
 */
public class ConfigurationManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationManager.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);
    private static final Map<String, Class<? extends ValidationRule>> RULE_TYPES = new KeyMap<>();

    static {
        RULE_TYPES.put("Condition", ConditionalValidationRule.class);
        RULE_TYPES.put("Find",      FindValidationRule.class);
        RULE_TYPES.put("Lookup",    LookupValidationRule.class);
        RULE_TYPES.put("Match",     MatchValidationRule.class);
        RULE_TYPES.put("Metadata",  MetadataValidationRule.class);
        RULE_TYPES.put("Unique",    UniqueValueValidationRule.class);
        RULE_TYPES.put("Regex",     RegularExpressionValidationRule.class);
        RULE_TYPES.put("Required",  ConditionalRequiredValidationRule.class);
        RULE_TYPES.put("Property",  DomainPropertyValidationRule.class);
        RULE_TYPES.put("Varlength", VariableLengthValidationRule.class);
        RULE_TYPES.put("Varorder",  VariableOrderValidationRule.class);

        List<Class<ValidationRule>> external = Helpers.find(ValidationRule.class);

        for (Class<ValidationRule> type : external) {
            if (type.isAnnotationPresent(ValidationRule.RuleAssociation.class)) {
                ValidationRule.RuleAssociation annotation = type.getAnnotation(ValidationRule.RuleAssociation.class);

                String association = annotation.value();

                if (!RULE_TYPES.containsKey(association)) {
                    RULE_TYPES.put(association, type);

                    LOGGER.debug("Registred {} as rule association {}", type, association);
                } else {
                    LOGGER.error("Cannot register {} as rule association {} because the association is already defined",
                        type, association);
                }
            }
        }
    }

    private final Set<ConfigurationMetadata> metadata = new HashSet<>();
    private final Map<String, Template> configurations = new KeyMap<>();
    private final Map<String, Template> prototypes = new KeyMap<>();
    private final ValidationSession token;
    private final WritableRuleMetrics metrics;
    private final Set<RuleListener> ruleListeners;
    private final Map<String, String> defaults;

    public ConfigurationManager(ValidationSession token, WritableRuleMetrics metrics, Set<RuleListener> ruleListeners,
            Map<String, String> defaults) {
        this.token = token;
        this.metrics = metrics;
        this.defaults = defaults;
        this.ruleListeners = ruleListeners;
    }

    void complete() {
        for(Template configuration : this.configurations.values()) {
            configuration.complete();
        }
    }

    void define(Template configuration) {
        this.configurations.put(configuration.getTargetName(), configuration);
    }

    public boolean defines(String name) {
        return this.configurations.containsKey(name);
    }

    Template getConfiguration(String name) {
        return this.configurations.get(name);
    }

    public Set<ConfigurationMetadata> getMetadata() {
        return this.metadata;
    }

    Template getPrototype(String name) {
        return this.prototypes.get(name);
    }

    void register(ConfigurationMetadata metadata) {
        this.metadata.add(metadata);
    }

    void store(Definition rule) {
        Diagnostic.Type type = null;

        if (rule.hasProperty("Type")) {
            type = Diagnostic.Type.fromString(rule.getProperty("Type"));
        }

        DISPATCHER.dispatchTo(this.ruleListeners, l -> l::acceptTemplate, new RuleMetadata(
            rule.getProperty("ID"),
            rule.getProperty("PublisherID"),
            rule.getProperty("Category"),
            rule.getProperty("Message"),
            rule.getProperty("Description"),
            type
        ));
    }

    public Configuration prepare(String name, Set<String> variables, boolean createRules) {
        Template template = null;
        String ruleTemplateName = name;

        if (this.configurations.containsKey(name)) {
            // The configuration is named
            template = this.configurations.get(name);
        }

        if ((template == null || !template.isConfiguration()) && variables != null) {
            Template prototype = null;
            int matches = 0;

            // The configuration needs prototyping, for one reason or another
            for (Template candidate : this.prototypes.values()) {
                int count = candidate.matches(name, variables);

                if (count != 0 && (matches == 0 || count > matches)) {
                    matches = count;
                    prototype = candidate;
                }
            }

            if (prototype != null) {
                if (template == null) {
                    template = prototype;
                } else {
                    // Copy over the rules
                    for (SourceDetails.Reference target : SourceDetails.Reference.values()) {
                        for (RuleDefinition ruleDefinition : prototype.getRules(target)) {
                            template.defineRule(target, ruleDefinition);
                        }
                    }

                    // Supplement the variable information
                    prototype.updateVariables(template);

                    // Mark it as configured now so that we avoid the risk of
                    // repeating this step on a future iteration
                    template.markConfigured();
                    template.complete();
                }

                ruleTemplateName = prototype.getProperty("Name").toUpperCase();
            }
        }

        Configuration configuration = null;

        // It's possible that we weren't able to create anything from the prototype either,
        // so make sure we only create a Configuration for a configured Template
        if (template != null && template.isConfiguration()) {
            configuration = template.createFrom(name, variables);

            if (createRules) {
                // Now go through the process of creating the rules
                for (SourceDetails.Reference target : SourceDetails.Reference.values()) {
                    for (RuleDefinition ruleDefinition : template.getRules(target)) {
                        for (ValidationRule rule : this.prepareRule(ruleDefinition, configuration, target)) {
                            configuration.defineRule(target, rule);
                        }
                    }
                }
            }
        }

        if (configuration != null) {
            configuration.setProperty("Configuration", ruleTemplateName);
        }

        return configuration;
    }

    void prototype(Template prototype) {
        this.prototypes.put(prototype.getTargetName(), prototype);
    }

    public boolean prototypes(String name) {
        return this.prototypes.containsKey(name);
    }

    private ValidationRule instantiateRule(Configuration configuration, SourceDetails.Reference reference,
            RuleDefinition rule) throws Exception {
        Constructor<? extends ValidationRule> constructor;
        Class<? extends ValidationRule> ruleClass = RULE_TYPES.get(rule.getRuleType());

        constructor = ruleClass.getConstructor(RuleDefinition.class, ValidationSession.class,
            WritableRuleMetrics.Scope.class);

        return constructor.newInstance(
            rule,
            this.token,
            new WritableRuleMetrics.Scope(this.metrics, configuration.getTargetName(), reference)
        );
    }

    private List<RuleDefinition> performMagic(RuleDefinition rule, Configuration configuration)
            throws ConfigurationException {
        List<RuleDefinition> rules = new ArrayList<>(Collections.singleton(rule));

        for (MagicProperty magicProperty : MagicProperty.values()) {
            List<RuleDefinition> templates = new ArrayList<>(rules);

            rules.clear();

            for (RuleDefinition template : templates) {
                MagicVariableParser scope = new MagicVariableParser();
                MagicVariable magicVariable = null;

                // We actually only accept a single magic variable right now, but
                // theoretically we could allow multiple ones in the future...so set
                // ourselves up for that scenario so we can warn about lack of support if
                // someone tries to do it right now.
                for (String property : magicProperty.getProperties()) {
                    if (template.hasProperty(property)) {
                        List<MagicVariable> magicVariables = scope.parse(magicProperty,
                                String.format("%s.%s", template.getId(), property), template.getProperty(property));

                        if (magicVariables.size() > 1 || (magicVariables.size() != 0 && magicVariable != null)) {
                            throw new ConfigurationException(Type.RuleDefinition,
                                "Only one magic variable per rule definition is allowed");
                        }

                        if (magicVariables.size() > 0) {
                            magicVariable = magicVariables.get(0);
                        }
                    }
                }

                if (magicVariable != null) {
                    List<Definition> contextVariables = new ArrayList<>();

                    if (magicProperty == MagicProperty.Variables) {
                        for (Definition candidateVariable : configuration.getVariables()) {
                            if (magicVariable.matches(candidateVariable)) {
                                contextVariables.add(candidateVariable);
                            }
                        }
                    } else if (magicProperty == MagicProperty.Domains) {
                        for (String configurationName : this.configurations.keySet()) {
                            Definition candidateVariable = this.configurations.get(configurationName);

                            // This is a bit of a hack to support %Domain% and
                            // %Domain.From% as they were originally designed
                            Definition proxy = new Definition(Target.Domain, candidateVariable.getProperty("Name"));

                            proxy.setProperty("From", candidateVariable.getProperty("Name"));
                            proxy.setProperty("Name", configuration.getTargetName());

                            candidateVariable = Definition.createFrom(proxy, candidateVariable);

                            if (magicVariable.matches(candidateVariable)) {
                                contextVariables.add(candidateVariable);
                            }
                        }
                    }

                    if (!contextVariables.isEmpty()) {
                        rules.addAll(scope.prepare(template, magicVariable, contextVariables, this.defaults));
                    }
                } else {
                    rules.add(template);
                }
            }
        }

        return rules;
    }

    private List<ValidationRule> prepareRule(RuleDefinition rule, Configuration configuration,
            SourceDetails.Reference reference) {
        List<ValidationRule> rules = new ArrayList<>();
        String domain = configuration.getTargetName();

        for (String property : rule.getProperties()) {
            rule = rule.withProperty(property, rule.getProperty(property).replace("%Domain%", domain));
        }

        try {
            boolean autoDisplayDomainKeys = Boolean.parseBoolean(
                this.token.getOptions().getProperty("Parser.AutoDisplayDomainKeys"));
            for (RuleDefinition contextualRule : this.performMagic(rule, configuration)) {
                LOGGER.debug("Creating rule for {}: {}", domain, contextualRule);

                // TODO: We don't want this to happen for terminology, but we need a better way
                if (autoDisplayDomainKeys &&
                        !contextualRule.hasProperty("Display") &&
                        !contextualRule.getProperty("Category").equalsIgnoreCase("Terminology")) {
                    contextualRule = contextualRule.withProperty("Display", configuration.getProperty("DomainKeys"));
                }

                contextualRule = contextualRule.withSeverityFor(domain);

                ValidationRule createdRule = this.instantiateRule(configuration, reference, contextualRule);

                DISPATCHER.dispatchTo(this.ruleListeners, l -> l::acceptInstance, createdRule.getMetadata());

                rules.add(createdRule);
            }
        } catch (Exception ex) {
            LOGGER.error("Rule creation failed for an instance of {} in domain {}", rule.getId(), domain, ex);

            // TODO: Improve configuration error handling
            throw new RuntimeException(ex);
        }

        return rules;
    }

    public static boolean isValidRuleType(@Nonnull String type) {
        return RULE_TYPES.containsKey(type);
    }

    public static class ConfigurationMetadata {
        public final String standardName;
        public final String standardVersion;

        public ConfigurationMetadata(String standardName, String standardVersion) {
            this.standardName = standardName;
            this.standardVersion = standardVersion;
        }
    }
}
