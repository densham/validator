/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

class ValueList {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValueList.class);

    private final String oid;
    private final List<Clause> clauses = new LinkedList<>();

    ValueList(String oid) {
        this.oid = oid;
    }

    Clause addClause(String whereItemOid, String mandatory) {
        Clause clause = new Clause(whereItemOid, mandatory);

        this.clauses.add(clause);

        return clause;
    }

    String getOid() {
        return this.oid;
    }

    Resolver resolverFor(Definition variable) {
        return new Resolver(this, variable, variable.getPrefix());
    }

    static class Resolver {
        private final ValueList valueList;
        private final Definition variable;
        private final String prefix;

        private Resolver(ValueList valueList, Definition variable, String prefix) {
            this.valueList = valueList;
            this.variable = variable;
            this.prefix = prefix;
        }

        ValueList getValueList() {
            return this.valueList;
        }

        Definition getVariable() {
            return this.variable;
        }

        boolean resolve(Map<String, Definition> variables) {
            boolean result = true;
            // TODO: We're making an assumption about the prefix here which is true...
            //       but requires implementation knowledge outside of this code, which
            //       is bad. Need to change how that works to make this less problematic.
            this.variable.setPrefix(this.prefix);

            for (Clause clause : this.valueList.clauses) {
                Definition target = variables.get(clause.oid);
                String expression = clause.getExpression(variables);

                if (target == null || expression == null) {
                    result = false;

                    continue;
                }

                String name = target.getTargetName();

                this.variable.addDependency(Definition.createFrom(clause.oid, true, target)
                    .setProperty("Prefix", this.prefix)
                    .setProperty("Expression", expression)
                    .setProperty("Variable", name)
                    .setProperty("Mandatory", clause.getMandatoryFlag())
                ).setProperty("@Clause", "Y");
            }

            this.variable.clearPrefix();

            return result;
        }
    }

    static class Clause {
        private final String oid;
        private final boolean isMandatory;
        private final List<Check> checks = new LinkedList<>();

        private Clause(String whereItemOid, String mandatory) {
            this.oid = whereItemOid;
            this.isMandatory = "yes".equalsIgnoreCase(mandatory);
        }

        public Clause addCheck(String checkItemOid, String comparator, Set<String> values) {
            this.checks.add(new Check(checkItemOid, comparator, values));

            return this;
        }

        String getMandatoryFlag() {
            return this.isMandatory ? "Y" : "N";
        }

        String getExpression(Map<String, Definition> variables) {
            if (this.checks.isEmpty()) {
                return null;
            }

            StringBuilder buffer = new StringBuilder();
            boolean shouldWrap = this.checks.size() > 1;
            boolean isFirst = true;

            for (Check check : this.checks) {
                if (!isFirst || (isFirst = false)) {
                    buffer.append(" @and ");
                }

                if (shouldWrap) {
                    buffer.append("(");
                }

                String resolved = check.resolve(variables);

                if (resolved == null) {
                    // TODO: Better error handling...
                    return null;
                }

                buffer.append(resolved);

                if (shouldWrap) {
                    buffer.append(")");
                }
            }

            return buffer.toString();
        }

        private static class Check {
            private static final ThreadLocal<Set<String>> COMPARATORS = new ThreadLocal<Set<String>>() {
                @Override
                protected Set<String> initialValue() {
                    return new HashSet<>(Arrays.asList(
                        "lt", "le", "gt", "ge", "eq", "ne", "in", "notin"
                    ));
                }
            };
            private final String oid;
            private final boolean matchAll;
            private final boolean isValid;
            private final List<String> expressions = new LinkedList<>();

            Check(String oid, String comparator, Set<String> values) {
                this.oid = oid;
                comparator = comparator.trim().toLowerCase();

                boolean isValid = true;

                if (!COMPARATORS.get().contains(comparator)) {
                    LOGGER.warn("Unknown RangeCheck operator {}", comparator.toUpperCase());

                    isValid = false;
                }

                if (values.size() > 1 && !comparator.equals("in") && !comparator.equals("notin")) {
                    LOGGER.warn("RangeCheck Operator {} cannot have multiple values", comparator.toUpperCase());

                    isValid = false;
                }

                this.isValid = isValid;
                this.matchAll = comparator.equals("notin");

                if (this.isValid) {
                    String operator = "==";

                    if (comparator.equals("ne") || comparator.equals("notin")) {
                        operator = "!=";
                    } else if (!comparator.equals("eq") && !comparator.equals("in")) {
                        operator = "@" + comparator;
                    }

                    for (String value : values) {
                        this.expressions.add(" " + operator + " '" + value.replace("'", "\\'") + "'");
                    }
                }
            }

            String resolve(Map<String, Definition> variables) {
                if (!this.isValid) {
                    return null;
                }

                Definition variable = variables.get(this.oid);

                if (variable == null) {
                    // TODO: Better error handling...
                    return null;
                }

                boolean isFirst = true;
                StringBuilder buffer = new StringBuilder();

                for (String expression : this.expressions) {
                    // TODO: We could just have an IN operator in the expressions...
                    //       but we don't for right now, so ors it is
                    if (!isFirst || (isFirst = false)) {
                        buffer.append(this.matchAll ? " @and " : " @or ");
                    }

                    buffer.append(variable.getTargetName()).append(expression);
                }

                return buffer.toString();
            }
        }
    }
}
