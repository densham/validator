/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import net.pinnacle21.validator.util.Hex;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
class MagicVariableParser {
    enum MagicProperty {
        Variables(
            "If",
            "Terms",
            "Test",
            "Variable",
            "When",
            "GroupBy"
        ),
        Domains(
            "From",
            "Terms"
        );

        private final String[] properties;

        MagicProperty(String...properties) {
            this.properties = properties;
        }

        String[] getProperties() {
            return this.properties;
        }

        String getSingular() {
            String singular = this.toString();

            return singular.substring(0, singular.length() - 1);
        }
    }

    private final static String CONTEXT_PROPERTY = "VariableContext";

    private final Pattern clause = Pattern.compile("[A-Za-z0-9!,;*#@_]+");
    private final Pattern token = Pattern.compile("(?:^| |'|\\()([=+]?%[^%]+%)");

    List<MagicVariable> parse(MagicProperty property, String name, String parsable) {
        Matcher tokenizer = this.token.matcher(parsable);
        List<MagicVariable> magicVariables = new ArrayList<>();

        while (tokenizer.find()) {
            String magicName = property.toString();
            String original = tokenizer.group(1);
            String token = original.substring(original.indexOf('%') + 1,
                    original.length() - 1);

            if (token.startsWith(magicName)) {
                token = token.substring(magicName.length());
                String clause;
                String[] tokens = token.split("\\$");
                List<String> conditions = new ArrayList<>();
                List<List<String>> clauses = new ArrayList<>();

                // The first token is a special case for two reasons:
                //
                //   1. Backward compatibility : The original format was based on
                //          %Variables[*].Property.Subproperty%, which we want to
                //          preserve support for.
                //   2. Variable clause ([*])  : This can appear at the beginning,
                //          so it appears in the first token here after the split.
                //

                // Okay, now it's three reasons. We're moving the source of the variable
                // to the beginning of the condition to make it easier to identify, i.e.
                //
                //   3. Variable source (:<Source>)
                boolean isScoped = tokens[0].startsWith(":");

                if (isScoped) {
                    int end = tokens[0].indexOf('[');

                    if (end == -1) {
                        end = tokens[0].indexOf('.');

                        if (end == -1) {
                            end = tokens[0].length();
                        }
                    }

                    conditions.add(tokens[0].substring(1, end));

                    tokens[0] = tokens[0].substring(end);
                }

                if (tokens[0].startsWith("[")) {
                    int end = tokens[0].indexOf(']');

                    if (end != -1) {
                        clause = tokens[0].substring(1, end);

                        if (this.clause.matcher(clause).matches()) {
                            tokens[0] = tokens[0].substring(end + 1);
                            String[] pieces = clause.split(";");

                            for (String piece : pieces) {
                                clauses.add(Arrays.asList(piece.split(",")));
                            }
                        } else {
                            throw new MagicVariableSyntaxException(name, original, "Invalid variable clause syntax");
                        }
                    } else {
                        throw new MagicVariableSyntaxException(name, original, "Missing closing ]");
                    }
                }

                if (tokens[0].startsWith(".")) {
                    tokens[0] = tokens[0].substring(1);
                }

                for (String identifier : tokens) {
                    if (identifier.length() > 0) {
                        // TODO: This should be phased out by changing the applicable rules
                        if (identifier.contains("Define")) {
                            isScoped = true;
                        }

                        conditions.add(identifier);
                    }
                }

                if (!isScoped) {
                    conditions.add("Config");
                }

                magicVariables.add(new MagicVariable(property, original, clauses,
                        conditions));
            }
        }

        return magicVariables;
    }

    List<RuleDefinition> prepare(final RuleDefinition rule, MagicVariable magicVariable,
            List<Definition> candidateVariables, Map<String, String> defaults) {
        List<RuleDefinition> preparedRules = new ArrayList<>();

        if (magicVariable.isReplicated()) {
            for (Definition candidateVariable : candidateVariables) {
                List<Definition> normalizedCandidates = new LinkedList<>();

                if (magicVariable.isDependencyReplicated()) {
                    for (Definition dependency : candidateVariable.getDependencies()) {
                        if (magicVariable.matchesClause(dependency)) {
                            String prefix = dependency.getProperty("Prefix");
                            String context = Hex.sha1(dependency.getProperty("Expression"))
                                .substring(0, 6)
                                .toUpperCase();

                            if (StringUtils.isNotEmpty(prefix)) {
                                prefix += ".";
                            }

                            normalizedCandidates.add(candidateVariable.with(prefix + "@Clause", dependency)
                                .setProperty(CONTEXT_PROPERTY, context));
                        }
                    }
                } else {
                    normalizedCandidates.add(candidateVariable);
                }

                for (Definition normalizedCandidate : normalizedCandidates) {
                    RuleDefinition contextualRule = rule.withContext(normalizedCandidate);

                    String name = normalizedCandidate.getTargetName().toUpperCase();
                    String context = name;

                    if (normalizedCandidate.hasProperty(CONTEXT_PROPERTY)) {
                        context += "." + normalizedCandidate.getProperty(CONTEXT_PROPERTY);
                    }

                    preparedRules.add(
                        this.replaceProperties(
                            this.replaceVariable(
                                contextualRule,
                                magicVariable,
                                name,
                                context
                            ),
                            magicVariable,
                            normalizedCandidate,
                            defaults
                        )
                    );
                }
            }
        } else {
            StringBuilder builder = new StringBuilder();
            RuleDefinition contextualRule = rule;
            Definition previous = null;
            String name;

            for (Definition candidate : candidateVariables) {
                if (previous != null) {
                    name = previous.getTargetName().toUpperCase();
                    builder.append(name).append(",");
                }

                previous = candidate;

                contextualRule = contextualRule.withContext(candidate);
            }

            if (previous != null) {
                builder.append(previous.getTargetName().toUpperCase());
            }

            preparedRules.add(
                this.replaceVariable(
                    contextualRule,
                    magicVariable,
                    builder.toString(),
                    null
                )
            );
        }

        return preparedRules;
    }

    private RuleDefinition replaceProperties(RuleDefinition rule, MagicVariable magicVariable,
            Definition candidateVariable, Map<String, String> defaults) {
        MagicProperty magicProperty = magicVariable.getMagicProperty();

        Pattern token = Pattern.compile(
            "%" + magicProperty.getSingular() + "\\.([^%|]+)(?:\\|([^%]+))?%"
        );

        for (String property : rule.getProperties()) {
            String value = rule.getProperty(property);
            Matcher tokenizer = token.matcher(value);

            while (tokenizer.find()) {
                String expected = tokenizer.group(1);
                String fallback = tokenizer.group(2);

                if (candidateVariable.hasProperty(expected)) {
                    value = value.replace(
                        tokenizer.group(0), candidateVariable.getProperty(expected)
                    );

                    continue;
                }

                if (fallback != null) {
                    String replacement = null;

                    if (fallback.startsWith("System.")) {
                        replacement = defaults.get(
                            fallback.substring("System.".length()).toLowerCase()
                        );
                    } else if (candidateVariable.hasProperty(fallback)) {
                        replacement = candidateVariable.getProperty(fallback);
                    }

                    if (replacement != null) {
                        value = value.replace(tokenizer.group(0), replacement);
                    }
                }
            }

            Map<Integer, String> references = magicVariable.getReferences(candidateVariable);

            if (references != null) {
                for (int i : references.keySet()) {
                    value = value.replace(
                        "%" + magicProperty.getSingular() + "." + i + "%",
                        references.get(i)
                    );
                }
            }

            value = value.replace(
                "%" + magicProperty.getSingular() + "%",
                candidateVariable.getTargetName()
            );

            rule = rule.withProperty(property, value);
        }

        return rule;
    }

    private RuleDefinition replaceVariable(RuleDefinition rule, MagicVariable magicVariable, String replacement,
            String context) {
        for (String property : magicVariable.getMagicProperty().getProperties()) {
            if (rule.hasProperty(property)) {
                rule = rule.withProperty(property, rule.getProperty(property).replace(
                    magicVariable.getIdentifier(), replacement
                ));
            }
        }

        if (context != null) {
            rule = rule.withContext(context);
        }

        return rule;
    }
}
