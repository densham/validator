/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author Tim Stone
 */
public class ConcurrentCacheMap<T> implements ConcurrentMap<String, T> {
    private final Map<String, ConcurrentCacheMap<T>.SoftKeyedReference> cache = new KeyMap<>();
    private final Queue<T> lastHardReferences;
    private final ReferenceQueue<T> queuedReferences = new ReferenceQueue<>();

    public ConcurrentCacheMap() {
        this(1);
    }

    public ConcurrentCacheMap(int retainerSize) {
        this.lastHardReferences = new BoundedQueue<>(retainerSize);
    }

    public synchronized void clear() {
        this.cache.clear();
        this.lastHardReferences.clear();
    }

    public synchronized boolean containsKey(Object key) {
        return this.cache.containsKey(key);
    }

    public synchronized boolean containsValue(Object value) {
        return this.cache.containsValue(value);
    }

    public synchronized Set<Map.Entry<String, T>> entrySet() {
        throw new UnsupportedOperationException("No entrySet can be generated at this time");
    }

    public synchronized T get(Object key) {
        T value = null;

        if (this.cache.containsKey(key)) {
            value = this.cache.get(key).get();
        }

        this.clean();

        return value;
    }

    public synchronized boolean isEmpty() {
        this.clean();

        return this.cache.isEmpty();
    }

    public synchronized Set<String> keySet() {
        this.clean();

        return this.cache.keySet();
    }

    public synchronized T put(String key, T value) {
        SoftKeyedReference reference = new SoftKeyedReference(key, value);

        this.lastHardReferences.offer(value);
        reference = this.cache.put(key, reference);
        this.clean();

        return (reference == null ? null : reference.get());
    }

    public synchronized void putAll(Map<? extends String, ? extends T> values) {
        for (Map.Entry<? extends String, ? extends T> entry : values.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }

    public synchronized T putIfAbsent(String key, T value) {
        if (!this.containsKey(key)) {
            value = this.put(key, value);
        } else {
            value = this.get(key);
        }

        return value;
    }

    public synchronized boolean remove(Object key, Object value) {
        boolean result = false;

        if (this.containsKey(key) && value.equals(this.get(key))) {
            this.cache.remove(key);
            result = true;
        }

        return result;
    }

    public synchronized T replace(String key, T replacement) {
        T result = null;

        if (this.containsKey(key)) {
            result = this.put(key, replacement);
        }

        return result;
    }

    public synchronized boolean replace(String key, T value, T replacement) {
        boolean result = false;

        if (this.containsKey(key) && value.equals(this.get(key))) {
            this.put(key, replacement);

            result = true;
        }

        return result;
    }

    public synchronized T remove(Object key) {
        T value = null;

        if (this.containsKey(key)) {
            value = this.cache.get(key).get();

            this.lastHardReferences.remove(key);
        }

        this.clean();

        return value;
    }

    public synchronized int size() {
        return this.cache.size();
    }

    public synchronized Collection<T> values() {
        Collection<T> values = new ArrayList<>();

        this.clean();

        for (SoftKeyedReference reference : this.cache.values()) {
            T value = reference.get();

            if (value != null) {
                values.add(value);
            }
        }

        return values;
    }

    private class SoftKeyedReference extends SoftReference<T> {
        private final String key;

        private SoftKeyedReference(String key, T value) {
            super(value, ConcurrentCacheMap.this.queuedReferences);

            this.key = key;
        }

        private String getKey() {
            return this.key;
        }
    }

    @SuppressWarnings("unchecked")
    private void clean() {
        Reference<? extends T> reference;

        while ((reference = this.queuedReferences.poll()) != null) {
            if (reference instanceof ConcurrentCacheMap.SoftKeyedReference) {
                SoftKeyedReference keyedReference = (SoftKeyedReference)reference;

                this.cache.remove(keyedReference.getKey());
            }
        }
    }
}
