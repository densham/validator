/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import net.pinnacle21.validator.api.events.ValidationEvent;

public final class ValidationEvents {
    private static final int NULL_INT = -1;

    private ValidationEvents() {}

    public static ValidationEvent configurationStartEvent(String configurationPath) {
        return new ValidationEventImpl(ValidationEvent.Type.Configuring, ValidationEvent.State.Start,
                configurationPath, NULL_INT, NULL_INT, null);
    }

    public static ValidationEvent configurationStopEvent(String configurationPath) {
        return new ValidationEventImpl(ValidationEvent.Type.Configuring, ValidationEvent.State.Stop,
                configurationPath, NULL_INT, NULL_INT, null);
    }

    public static ValidationEvent processingStartEvent(String name, long currentTask, long totalTasks) {
        return new ValidationEventImpl(ValidationEvent.Type.Processing, ValidationEvent.State.Start,
                name, currentTask, totalTasks, null);
    }

    public static ValidationEvent processingIncrementEvent(String name, long currentTask, long totalTasks,
            long recordCount) {
        ValidationEvent subevent = new ValidationEventImpl(ValidationEvent.Type.Processing,
                ValidationEvent.State.InProgress, name, recordCount, NULL_INT, null);

        return new ValidationEventImpl(ValidationEvent.Type.Processing, ValidationEvent.State.InProgress, name,
                currentTask, totalTasks, subevent);
    }

    public static ValidationEvent processingStopEvent(String name, long currentTask, long totalTasks) {
        return new ValidationEventImpl(ValidationEvent.Type.Processing, ValidationEvent.State.Stop,
                name, currentTask, totalTasks, null);
    }

    public static ValidationEvent subprocessingStartEvent(String name) {
        return new ValidationEventImpl(ValidationEvent.Type.Subprocessing, ValidationEvent.State.Start, name,
                NULL_INT, NULL_INT, null);
    }

    public static ValidationEvent subprocessingIncrementEvent(String name, long recordCount) {
        return new ValidationEventImpl(ValidationEvent.Type.Subprocessing, ValidationEvent.State.InProgress, name,
                recordCount, NULL_INT, null);
    }

    public static ValidationEvent subprocessingStopEvent(String name) {
        return new ValidationEventImpl(ValidationEvent.Type.Subprocessing, ValidationEvent.State.Stop, name,
                NULL_INT, NULL_INT, null);
    }

    private static class ValidationEventImpl implements ValidationEvent {
        private final long timestamp;
        private final long current;
        private final long maximum;
        private final String name;
        private final Type type;
        private final State state;
        private final ValidationEvent subevent;

        private ValidationEventImpl(Type type, State state, String name, long current, long maximum,
                ValidationEvent event) {
            this.timestamp = System.currentTimeMillis();
            this.type = type;
            this.state = state;
            this.name = name;
            this.current = current;
            this.maximum = maximum;
            this.subevent = event;
        }

        @Override
        public long getCurrent() {
            return this.current;
        }

        @Override
        public long getMaximum() {
            return this.maximum;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public State getState() {
            return this.state;
        }

        @Override
        public ValidationEvent getSubevent() {
            return this.subevent;
        }

        @Override
        public long getTimestamp() {
            return this.timestamp;
        }

        @Override
        public Type getType() {
            return this.type;
        }
    }
}
