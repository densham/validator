/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.report;

import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class DiagnosticImpl implements Diagnostic {
    private final RuleMetadata metadata;
    private final SourceDetails sourceDetails;
    private final DataDetails dataDetails;
    private final long creationTimestamp;
    private final Map<String, String> values;

    public DiagnosticImpl(RuleMetadata metadata, SourceDetails sourceDetails, DataDetails dataDetails,
            Map<String, String> values) {
        this.metadata = metadata;
        this.sourceDetails = sourceDetails;
        this.dataDetails = dataDetails;
        this.creationTimestamp = System.currentTimeMillis();
        this.values = Collections.unmodifiableMap(values);
    }

    @Override
    public String getId() {
        return this.metadata.id;
    }

    @Override
    public String getMessage() {
        return this.metadata.message;
    }

    @Override
    public String getDescription() {
        return this.metadata.description;
    }

    @Override
    public String getCategory() {
        return this.metadata.category;
    }

    @Override
    public Type getType() {
        return this.metadata.type;
    }

    @Override
    public String getContext() {
        return this.metadata.context;
    }

    @Override
    public long getTimestamp() {
        return this.creationTimestamp;
    }

    @Override
    public SourceDetails getSourceDetails() {
        return this.sourceDetails;
    }

    @Override
    public DataDetails getDataDetails() {
        return this.dataDetails;
    }

    @Override
    public Set<String> getVariables() {
        return this.values.keySet();
    }

    @Override
    public Map<String, String> getVariableValues() {
        return this.values;
    }

    @Override
    public String getVariable(String variable) {
        return this.values.get(variable);
    }

    @Override
    public Set<String> getProperties() {
        return Collections.emptySet();
    }

    @Override
    public String getProperty(String property) {
        return null;
    }

    @Override
    public Map<String, String> getPropertyValues() {
        return Collections.emptyMap();
    }
}
