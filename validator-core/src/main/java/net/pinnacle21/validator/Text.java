/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author Tim Stone
 */
public final class Text {
    private static boolean ATTEMPTED = false;
    private static final String BUNDLE_NAME = "validator-core-text";
    private static final String DEFAULT_LANG = "en";
    private static ResourceBundle RESOURCE_BUNDLE = null;

    /**
     * Gets a string from the resource file.
     *
     * @param key
     *
     * @return The text associated with the given <code>key</code>.
     */
    public static String get(String key) {
        if (ATTEMPTED && RESOURCE_BUNDLE == null) {
            return '!' + key + '!';
        }

        if (RESOURCE_BUNDLE == null) {
            try {
                Locale locale   = new Locale(DEFAULT_LANG);
                RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, locale);
            } catch (MissingResourceException ex) {
                ATTEMPTED = true;

                return Text.get(key);
            }
        }

        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException ex) {
            return '!' + key + '!';
        }
    }
}
