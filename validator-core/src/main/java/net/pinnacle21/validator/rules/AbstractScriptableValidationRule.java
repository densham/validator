/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.rules.expressions.EvaluationException;
import net.pinnacle21.validator.rules.expressions.Expression;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.SupplementalDataRecord;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
public abstract class AbstractScriptableValidationRule extends AbstractValidationRule {
    protected static final String DEFAULT_EXPRESSION_NAME = "DEFAULT_EXPRESSION";

    private final ImmutableMap<String, DataEntry> nullableValues;
    private final Map<String, Expression> expressions = new HashMap<>();
    private boolean hasExpressions = false;

    protected AbstractScriptableValidationRule(RuleDefinition definition, ValidationSession token, Target target,
        String[] required, WritableRuleMetrics.Scope metrics)
            throws ConfigurationException {
        super(definition, token, target, required, metrics);

        ImmutableMap.Builder<String, DataEntry> values = ImmutableMap.builder();

        if (definition.hasProperty("Optional")) {
            String[] nullableVariables = definition.getProperty("Optional")
                    .split(Pattern.quote(","));

            for (String nullableVariable : nullableVariables) {
                values.put(nullableVariable.trim().toUpperCase(), DataEntry.NULL_ENTRY);
            }
        }

        this.nullableValues = values.build();
    }

    public boolean validate(DataRecord dataRecord, Consumer<Diagnostic> reporter) throws CorruptRuleException {
        if (!this.nullableValues.isEmpty()) {
            dataRecord = new SupplementalDataRecord(dataRecord, this.nullableValues, true);
        }

        return super.validate(dataRecord, reporter);
    }

    protected void addVariable(String variable) {
        variable = variable.toUpperCase();

        if (!this.nullableValues.containsKey(variable)) {
            super.addVariable(variable);
        }
    }

    protected boolean checkExpression(DataRecord dataRecord)
            throws CorruptRuleException {
        return this.checkExpression(dataRecord, DEFAULT_EXPRESSION_NAME);
    }

    protected boolean checkExpression(DataRecord dataRecord, String name)
            throws CorruptRuleException {
        Expression expression = this.expressions.get(name);

        if (expression == null) {
            return true;
        }

        try {
            return expression.evaluate(dataRecord);
        } catch (EvaluationException ex) {
            throw new CorruptRuleException(
                CorruptRuleException.State.Temporary,
                super.getID(),
                ex.getMessage(),
                ex.getDescription()
            );
        }
    }

    protected boolean hasExpression() {
        return this.hasExpression(DEFAULT_EXPRESSION_NAME);
    }

    protected boolean hasExpression(String name) {
        return this.hasExpressions && this.expressions.containsKey(name);
    }

    protected void prepareExpression(String condition) {
        this.prepareExpression(DEFAULT_EXPRESSION_NAME, condition);
    }

    protected void prepareExpression(String condition, boolean registerVariables) {
        this.prepareExpression(DEFAULT_EXPRESSION_NAME, condition, registerVariables);
    }

    protected void prepareExpression(String name, String testCondition) {
        this.prepareExpression(name, testCondition, true);
    }

    protected void prepareExpression(String name, String testCondition, boolean registerVariables) {
        if (testCondition == null) {
            throw new IllegalArgumentException("testCondition cannot be null");
        }

        Expression compiled = Expression.createFrom(testCondition, this.session.getOptions(),
                this.session.getDataEntryFactory());

        if (registerVariables) {
            for (String variable : compiled.getVariables()) {
                this.addVariable(variable);
            }
        }

        this.hasExpressions = true;
        this.expressions.put(name, compiled);
    }

    protected Map<String, String> pullRecordValues(DataRecord record, Set<String> variables) {
        return super.pullRecordValues(record, Sets.union(variables, this.nullableValues.keySet()));
    }
}
