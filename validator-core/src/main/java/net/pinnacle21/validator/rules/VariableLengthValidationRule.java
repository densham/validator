/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.VariableDetails;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.Definition;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.*;

/**
 * @author Tim Stone
 */
public class VariableLengthValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private final int min;
    private final int max;
    private final Map<String, Integer> lengths = new HashMap<>();
    private final boolean countExcess;

    public VariableLengthValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Dataset, REQUIRED_VARIABLES, metrics);

        for (Definition variable : definition.getContexts()) {
            this.lengths.put(variable.getTargetName().toUpperCase(), 0);
        }

        if (!definition.hasProperty("Minimum") && !definition.hasProperty("Maximum")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "VariableLength rules must have one of (Minimum, Maximum)"
            );
        }

        int min = 0;
        int max = -1;

        if (definition.hasProperty("Minimum")) {
            try {
                min = Integer.parseInt(definition.getProperty("Minimum"));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Invalid value for Minimum, expected number",
                    ex
                );
            }
        }

        if (definition.hasProperty("Maximum")) {
            try {
                max = Integer.parseInt(definition.getProperty("Maximum"));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Invalid value for Maximum, expected number",
                    ex
                );
            }
        }

        this.min = Math.max(0, min);
        this.max = max;
        this.countExcess = !definition.getProperty("Count").equalsIgnoreCase("Length");
    }

    protected byte performValidation(DataRecord dataRecord) {
        for (String variable : this.lengths.keySet()) {
            if (!dataRecord.definesVariable(variable)) {
                continue;
            }

            DataEntry entry = dataRecord.getValue(variable);

            if (entry.hasValue()) {
                int length = entry.toString().length();
                int current = this.lengths.get(variable);

                if (length > current) {
                    this.lengths.put(variable, length);
                }
            }
        }

        return -1;
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        List<SourceDetails> entities = entity.hasProperty(SourceDetails.Property.Combined)
            ? entity.getSplitSources()
            : Collections.singletonList(entity);
        Map<String, Integer> maximums = new HashMap<>();

        for (SourceDetails dataset : entities) {
            for (String name : this.lengths.keySet()) {
                VariableDetails variable = dataset.getVariable(name);

                if (variable != null && variable.hasProperty(VariableDetails.Property.Length)) {
                    int length = variable.getInteger(VariableDetails.Property.Length);
                    Integer max = maximums.get(name);

                    if (max == null || max < length) {
                        maximums.put(name, length);
                    }
                }
            }
        }

        List<Outcome> results = new ArrayList<>();

        for (String variable : this.lengths.keySet()) {
            Integer max = maximums.get(variable);
            Outcome result;

            if (max != null || !this.countExcess) {
                int length = this.lengths.get(variable);
                boolean failed;

                if (this.countExcess) {
                    if (length > 0) {
                        length = max - length;
                        failed = length >= this.min &&
                            (length <= this.max || this.max == -1);
                    } else {
                        failed = false;
                    }
                } else {
                    failed = length < this.min || (this.max != -1 && length > this.max);
                }

                result = new Outcome((byte)(!failed ? 1 : 0));

                if (failed) {
                    result.display.put("Variable", variable);
                    result.display.put(this.countExcess ? "Excess" : "Length" , Integer.toString(length));
                }
            } else {
                result = new Outcome((byte)-1);
            }

            results.add(result);
        }

        return results;
    }
}
