/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.rules.expressions.functions.Function;
import net.pinnacle21.validator.rules.expressions.functions.Functions;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;

/**
 *
 * @author Tim Stone
 */
class Comparison implements Evaluable {
    private static final double DEFAULT_EPSILON = 0.001;
    private static final String VARIABLE_PATTERN = "[A-Za-z][A-Za-z0-9]*|(?:VAL|SUB|VAR):[A-Za-z0-9]+";

    private final String lhs;
    private final String rhs;
    private final DataEntry constant;
    private final String operator;
    private final Function function;
    private final boolean equal;
    private final boolean greater;
    private final boolean less;
    private final double epsilon;

    static Evaluable createComparison(String expression, ValidationOptions options, DataEntryFactory factory) {
        Evaluable comparison;

        expression = expression.trim();

        Pattern comparisonPattern = Pattern.compile(
            "(" + VARIABLE_PATTERN + ")" +
            "(?:" +
              "\\s*" +
              "([<>]|[%.^=<>!~]=)" +
              "\\s*" +
            ")" +
            "(" +
              "(?:" + VARIABLE_PATTERN + ")" +
              "|" +
              "'(?:[^']|\\\\')+'" +
              "|" +
              "(?:-?[0-9]+(?:\\.[0-9]+)?)" +
              "|" +
              "null" +
              "|" +
              ":[A-Z]+\\([^)]+\\)" +
            ")"
        );
        Matcher comparisonMatcher = comparisonPattern.matcher(expression);

        // TODO: Provide more useful information about the location/type of syntax error
        if (!comparisonMatcher.matches() || comparisonMatcher.groupCount() != 3) {
            throw new SyntaxException(String.format(
                "The comparison clause %s not properly formatted",
                expression
            ));
        }

        String lhs = comparisonMatcher.group(1);
        String rhs = comparisonMatcher.group(3);
        String operator = comparisonMatcher.group(2);

        // Replace escaped single quotes
        rhs = rhs.replace("\\'", "'");

        if (rhs.equals("null")) {
            boolean isNegated = false;

            // This is a NullComparison
            if (operator.equals("!=")) {
                isNegated = true;
            } else if (!operator.equals("==")) {
                throw new SyntaxException(String.format(
                    "The comparison clause %s attempts to compare the variable %s "
                    + "to null via an incompatible operator (%s)",
                    expression,
                    lhs,
                    operator
                ));
            }

            comparison = new NullComparison(lhs, isNegated);
        } else if (operator.equals("~=")) {
            comparison = new RegexComparison(lhs, rhs.substring(1, rhs.length() - 1));
        } else {
            double epsilon = DEFAULT_EPSILON;

            if (options.hasProperty("Engine.FuzzyTolerance")) {
                try {
                    epsilon = Double.parseDouble(options.getProperty("Engine.FuzzyTolerance"));
                } catch (NumberFormatException ignore) {}
            }

            comparison = new Comparison(lhs, rhs, operator, epsilon, factory);
        }

        return comparison;
    }

    private Comparison(String lhs, String rhs, String operator, double epsilon, DataEntryFactory factory) {
        this.lhs = lhs;
        this.operator = operator;
        this.equal =
            operator.equals("<=") || operator.equals(">=") || operator.equals("==");
        this.greater =
            operator.equals(">=") || operator.equals(">") || operator.equals("!=");
        this.less =
            operator.equals("<=") || operator.equals("<") || operator.equals("!=");
        this.epsilon = epsilon;

        if (rhs.matches(VARIABLE_PATTERN)) {
            // The right hand side is a variable
            this.rhs = rhs;
            this.constant = null;
            this.function = null;
        } else if (rhs.startsWith(":")) {
            // The right hand side is a function
            this.rhs = null;
            this.constant = null;
            this.function = Functions.create(rhs, factory);
        } else {
            // The right hand side is a constant
            this.rhs = null;

            if (rhs.startsWith("'")) {
                rhs = rhs.substring(1, rhs.length() - 1);
            }

            this.constant = factory.create(rhs);
            this.function = null;
        }
    }

    public boolean evaluate(DataRecord record) {
        boolean result;
        DataEntry lhs = record.getValue(this.lhs);
        DataEntry rhs;
        String operator = this.operator;
        int comparison;

        if (this.constant == null) {
            if (this.function == null) {
                rhs = record.getValue(this.rhs);
            } else {
                rhs = this.function.compute(record);
            }
        } else {
            rhs = this.constant;
        }

        switch (operator) {
            case "^=":
            case ".=":
                comparison = lhs.compareToAny(rhs, false);
                result = operator.equals("^=") ? comparison == 0 : comparison != 0;
                break;
            case "%=":
                if (lhs.isNumeric() && rhs.isNumeric()) {
                    BigDecimal lhsValue = (BigDecimal)lhs.getValue();
                    BigDecimal rhsValue = (BigDecimal)rhs.getValue();

                    result = nearlyEqual(lhsValue, rhsValue, this.epsilon);
                } else {
                    // Fall back to normal equality if the two sides aren't numbers
                    result = lhs.compareToAny(rhs, true) == 0;
                }
                break;
            default:
                comparison = lhs.compareToAny(rhs, true);

                if (comparison == 0) {
                    result = this.equal;
                } else if (comparison > 0) {
                    result = this.greater;
                } else {
                    result = this.less;
                }

                break;
        }

        return result;
    }

    public Set<String> getVariables() {
        Set<String> variables = new HashSet<>();

        variables.add(this.lhs);

        if (this.rhs != null) {
            variables.add(this.rhs);
        } else if (this.function != null) {
            variables.addAll(this.function.getVariables());
        }

        return variables;
    }

    public String toString() {
        String rhs = this.rhs;

        if (this.constant != null) {
            rhs = "'" + this.constant.toString() + "'";
        } else if (this.function != null) {
            rhs = this.function.toString();
        }

        return String.format("%s %s %s", this.lhs, this.operator, rhs);
    }

    private static class RegexComparison implements Evaluable {
        private final String lhs;
        private final Pattern pattern;
        private Matcher matcher = null;

        private RegexComparison(String lhs, String rhs) {
            this.lhs = lhs;
            this.pattern = Pattern.compile(rhs);
        }

        public boolean evaluate(DataRecord record) {
            DataEntry entry = record.getValue(this.lhs);
            String value = entry.toString();

            if (this.matcher == null) {
                this.matcher = this.pattern.matcher(value);
            } else {
                this.matcher.reset(value);
            }

            return this.matcher.matches();
        }

        public Set<String> getVariables() {
            return Collections.singleton(this.lhs);
        }

        public String toString() {
            return String.format("%s ~= '%s'", this.lhs, this.pattern.pattern());
        }
    }

    private static class NullComparison implements Evaluable {
        private final String lhs;
        private final boolean isNegated;

        private NullComparison(String lhs, boolean isNegated) {
            this.lhs = lhs;
            this.isNegated = isNegated;
        }

        public boolean evaluate(DataRecord record) {
            DataEntry entry = record.getValue(this.lhs);

            return this.isNegated == entry.hasValue();
        }

        public Set<String> getVariables() {
            return Collections.singleton(this.lhs);
        }

        public String toString() {
            return String.format("%s %s null", this.lhs, this.isNegated ? "!=" : "==");
        }
    }

    // https://stackoverflow.com/questions/4915462/how-should-i-do-floating-point-comparison/4915891#4915891
    private static boolean nearlyEqual(BigDecimal a, BigDecimal b, double epsilon) {
        if (a.compareTo(b) == 0) { // shortcut, handles infinities
            return true;
        } else {
            double dbA = a.doubleValue();
            double dbB = b.doubleValue();
            double absA = Math.abs(dbA);
            double absB = Math.abs(dbB);
            double diff = Math.abs(dbA - dbB);

            if (dbA == 0 || dbB == 0 || diff < Float.MIN_NORMAL) {
                // a or b is zero or both are extremely close to it
                // relative error is less meaningful here
                return diff < (epsilon * Float.MIN_NORMAL);
            } else { // use relative error
                return diff / (absA + absB) < epsilon;
            }
        }
    }
}
