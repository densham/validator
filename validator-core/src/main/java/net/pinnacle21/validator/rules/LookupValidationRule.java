/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.rules.expressions.PreparedQuery;
import net.pinnacle21.validator.Text;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.Lookup;
import net.pinnacle21.validator.data.LookupProvider;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.util.KeyMap;
import net.pinnacle21.validator.ValidationSession;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class LookupValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "From"
    };

    private final boolean ignoreWhereFailure;
    private final Map<String, Lookup> lookups = new KeyMap<>();
    private final LookupProvider provider;
    private final PreparedQuery query;

    public LookupValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        if (!definition.hasProperty("Variable") &&
                !definition.hasProperty("Search")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Lookup rules must have one of (Variable, Search)"
            );
        } else if (definition.hasProperty("Variable") &&
                definition.hasProperty("Search")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Lookup rules can only have one of (Variable, Search)"
            );
        }

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        this.ignoreWhereFailure = definition.hasProperty("WhereFailure") &&
                definition.getProperty("WhereFailure").equalsIgnoreCase("Ignore");

        String adapter = definition.hasProperty("Provider") ?
                definition.getProperty("Provider") : null;

        try {
            this.provider = this.session.getLookupProvider(adapter);
        } catch (IllegalArgumentException ex) {
            throw new ConfigurationException(ConfigurationException.Type.RuleDefinition, ex);
        }

        String search;
        String target = definition.getProperty("From");
        String where = definition.getProperty("Where");

        if (definition.hasProperty("Variable")) {
            // Upgrade legacy Variable syntax to new form (like where clause)
            search = definition.getProperty("Variable").replace(
                ",", " @and "
            ).replaceAll("(==\\s*)([A-Za-z+][A-Za-z0-9]+)", "$1 [$2]");
        } else {
            search = definition.getProperty("Search");
        }

        this.query = new PreparedQuery(target, search, where, this.session.getDataEntryFactory(), adapter == null);

        for (String variable : this.query.getLocal()) {
            super.addVariable(variable);
        }

        if(this.query.isRequestable()) {
            this.provider.request(this.query.getTarget(), this.query.getRemote());
        }
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        Set<String> lookupNames = new HashSet<>(this.lookups.keySet());

        for (String lookupName : lookupNames) {
            this.lookups.remove(lookupName);
        }

        return super.performDatasetValidation(entity);
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result = true;
        Set<String> remote = new HashSet<>();
        String target = this.query.getTarget(dataRecord);

        if (!this.lookups.containsKey(target) || this.lookups.get(target) != null) {
            Lookup lookup;
            List<PreparedQuery.Mapping> search = this.query.getSearch(dataRecord);
            List<PreparedQuery.Mapping> where = this.query.getWhere(dataRecord);

            // TODO: Iterating over these lists for this information seems annoying
            for (PreparedQuery.Mapping mapping : search) {
                remote.add(mapping.getRemote());
            }

            for (PreparedQuery.Mapping mapping : where) {
                remote.add(mapping.getRemote());
            }

            if (this.lookups.containsKey(target)) {
                lookup = this.lookups.get(target);

                if (!lookup.contains(remote)) {
                    lookup = this.provider.get(target, remote);

                    if (lookup == null) {
                        throw new CorruptRuleException(
                            CorruptRuleException.State.Temporary,
                            super.getID(),
                            String.format(
                                Text.get("Messages.MissingLookup"),
                                target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                            ),
                            String.format(
                                Text.get("Descriptions.MissingLookup"),
                                super.getID(),
                                target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                            )
                        );
                    } else {
                        this.lookups.put(target, lookup);
                    }
                }
            } else {
                lookup = this.provider.get(target, remote);

                this.lookups.put(target, lookup);

                if (lookup == null) {
                    throw new CorruptRuleException(
                        CorruptRuleException.State.Temporary,
                        super.getID(),
                        String.format(
                            Text.get("Messages.MissingLookup"),
                            target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                        ),
                        String.format(
                            Text.get("Descriptions.MissingLookup"),
                            super.getID(),
                            target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                        )
                    );
                }
            }

            result = lookup.seek(search, where, this.ignoreWhereFailure);
        }

        return (byte)(result ? 1 : 0);
    }
}
