/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.VariableDetails;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.Definition;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.util.Helpers;
import net.pinnacle21.validator.ValidationSession;

import java.util.*;

/**
 * @author Tim Stone
 */
public class VariableOrderValidationRule extends AbstractValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private final Map<String, Integer> ordering = new HashMap<>();
    private final Map<String, Set<String>> placed = new HashMap<>();

    public VariableOrderValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        super.addVariable("VARIABLE");
        super.addVariable("DATASET");

        for (Definition variable : definition.getContexts()) {
            String name = variable.getTargetName().toUpperCase();

            try {
                this.ordering.put(name, Integer.valueOf(variable.getProperty("Config.OrderNumber")));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    String.format(
                        "Invalid value for Order attribute of variable %s",
                        name
                    ),
                    ex
                );
            }
        }
    }

    public void setup(SourceDetails entity) {
        Map<String, List<Integer>> orders = new HashMap<>();
        Map<String, List<String>> namesets = new HashMap<>();

        List<SourceDetails> entities = entity.hasProperty(SourceDetails.Property.Combined)
            ? entity.getSplitSources()
            : Collections.singletonList(entity);

        for (SourceDetails dataset : entities) {
            // This rule is applied against the metadata, but we need the parent entity
            dataset = dataset.getParent();
            String datasetName = dataset.getString(SourceDetails.Property.Subname);
            Set<String> placed = this.placed.get(datasetName);

            if (placed == null) {
                this.placed.put(datasetName, placed = new HashSet<>());
                namesets.put(datasetName, new ArrayList<>());
                orders.put(datasetName, new ArrayList<>());
            }

            List<String> names = namesets.get(datasetName);
            List<Integer> order = orders.get(datasetName);

            for (VariableDetails variable : dataset.getVariables()) {
                String variableName = variable.getString(VariableDetails.Property.Name);
                Integer expected = this.ordering.get(variableName);

                if (expected != null) {
                    names.add(variableName);
                    order.add(expected);
                } else {
                    // An unknown variable is correctly placed because we have no idea
                    // where it *should* be, relative to anything else
                    placed.add(variableName);
                }
            }
        }

        for (String dataset : this.placed.keySet()) {
            Set<String> placed = this.placed.get(dataset);
            List<String> names = namesets.get(dataset);
            List<Integer> order = orders.get(dataset);

            int ordered[] = new int[names.size()];

            for (int i = 0; i < ordered.length; ++i) {
                ordered[i] = order.get(i);
            }

            ordered = Helpers.determineLargestIncreasingSubsequence(ordered, true);

            for (int index : ordered) {
                placed.add(names.get(index));
            }
        }
    }

    protected byte performValidation(DataRecord dataRecord) {
        Set<String> placed = this.placed.get(
            dataRecord.getValue("DATASET").toString().toUpperCase()
        );

        boolean result = placed.contains(
            dataRecord.getValue("VARIABLE").toString().toUpperCase()
        );

        return (byte)(result ? 1 : 0);
    }
}
