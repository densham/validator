/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.Text;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.expressions.EvaluationException;

import java.math.BigDecimal;

/**
 * @author Tim Stone
 */
public class SasDateTime extends AbstractFunction {
    private final static BigDecimal TIME_FACTOR = new BigDecimal(60 * 60 * 24);

    SasDateTime(String name, DataEntryFactory factory, String[] arguments) {
        super(name, factory, arguments, 1);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry timestamp = record.getValue(this.arguments[0]);

        if (!timestamp.hasValue() || !timestamp.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.TimestampNaN"),
                String.format(
                    Text.get("Descriptions.TimestampNaN"),
                    timestamp.getDataType().toString()
                )
            );
        }

        BigDecimal computed = (BigDecimal)timestamp.getValue();

        if (this.name.equals("TIME")) {
            computed = computed.remainder(TIME_FACTOR);
        } else {
            computed = computed.divideToIntegralValue(TIME_FACTOR);
        }

        return this.factory.create(computed);
    }
}
