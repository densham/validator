/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
public class RegularExpressionValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Variable", "Test"
    };

    private final Pattern expression;
    private final String variable;
    private Matcher matcher;

    public RegularExpressionValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        this.expression = Pattern.compile(definition.getProperty("Test"));
        this.variable = definition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        DataEntry entry = dataRecord.getValue(this.variable);

        if (entry.hasValue()) {
            String value = entry.toString();

            if (this.matcher == null) {
                this.matcher = this.expression.matcher(value);
            } else {
                this.matcher.reset(value);
            }

            if (!this.matcher.matches()) {
                return 0;
            }
        }

        return 1;
    }
}
