/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import com.google.common.collect.Sets;
import net.pinnacle21.validator.Text;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.SubjectDataSupplement;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.report.DiagnosticImpl;
import net.pinnacle21.validator.report.RuleMetadata;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

/**
 * Provides the framework necessary for all basic <code>{@link ValidationRule}</code>
 * implementations.
 *
 * @author Tim Stone
 */
public abstract class AbstractValidationRule implements ValidationRule {
    private final RuleMetadata metadata;
    private final Target target;
    private final WritableRuleMetrics.RuleMetric metrics;
    private final boolean isGenerating;
    private final boolean isUnqualifiedAllowed;
    private final byte negation;
    private final Set<String> excludes;
    private final Set<String> includes;

    protected final ValidationSession session;
    protected final Set<String> variables = new HashSet<>();

    protected AbstractValidationRule(RuleDefinition definition, ValidationSession session, Target target, String[] required,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        this.metadata = new RuleMetadata(definition, metrics.getDomain());
        this.target = target;
        this.session = session;
        this.isGenerating = !"true".equalsIgnoreCase(definition.getProperty("DisableMessaging"));
        this.isUnqualifiedAllowed = "true".equalsIgnoreCase(definition.getProperty("AllowUnqualifiedVariables"));
        this.negation = (byte)("false".equalsIgnoreCase(definition.getProperty("Expect")) ? 1 : 0);

        // Determine error message information
        this.metrics = metrics.getRule(this.metadata.id, this.metadata.context, this.metadata.message,
                this.metadata.type);

        // Extra variables to display in the report messages
        if (definition.hasProperty("Display")) {
            String[] variables = definition.getProperty("Display").split(Pattern.quote(","));
            Set<String> includes = new HashSet<>();
            Set<String> excludes = new HashSet<>();

            for (String variable : variables) {
                variable = variable.trim().toUpperCase();

                if (variable.startsWith("-")) {
                    excludes.add(variable.substring(1));
                } else {
                    if (variable.startsWith("+")) {
                        variable = variable.substring(1);
                    }

                    includes.add(variable);
                }
            }

            this.includes = includes.size() > 0 ? includes : null;
            this.excludes = excludes.size() > 0 ? excludes : null;
        } else {
            this.includes = null;
            this.excludes = null;
        }

        if (required != null) {
            String missingProperties = null;

            for (String requiredProperty : required) {
                if (!definition.hasProperty(requiredProperty)) {
                    if (missingProperties == null) {
                        missingProperties = requiredProperty;
                    } else {
                        missingProperties = missingProperties + ", " + requiredProperty;
                    }
                }
            }

            if (missingProperties != null) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    String.format(
                        Text.get("Exceptions.MissingAttribute"),
                        definition.getRuleType(),
                        this.getID(),
                        missingProperties
                    )
                );
            }
        }
    }

    public Target getTarget() {
        return this.target;
    }

    public RuleMetadata getMetadata() {
        return this.metadata;
    }

    public boolean validate(DataRecord dataRecord, Consumer<Diagnostic> reporter) throws CorruptRuleException {
        SourceDetails entity = dataRecord.getSourceDetails();

        if (this.isUnqualifiedAllowed) {
            dataRecord = new UnqualifiedReferenceDataRecord(dataRecord);
        }

        if (this.target == Target.Record) {
            this.metrics.start();
        }

        byte result;

        // TODO: It's probably sufficient to perform this check only once, instead of per-record...
        for (String variable : this.variables) {
            // Check if the variable is not defined
            if (!dataRecord.definesVariable(variable)) {
                // We can't perform validations with rules that don't have all their data
                // Consequently, mark that this rule is in an invalid state by throwing
                // an exception
                throw new CorruptRuleException(
                    CorruptRuleException.State.Unrecoverable,
                    this.getID(),
                    String.format(
                        Text.get("Messages.MissingVariables"),
                        this.getID(),
                        entity.getString(SourceDetails.Property.Name)
                    ),
                    Text.get("Descriptions.MissingVariables") + ": '" + variable + "'"
                );
            }
        }

        result = (byte)(this.performValidation(dataRecord) ^ this.negation);

        // Check if the rule failed and prepare the message if it did
        if (result == 0 && this.isGenerating && reporter != null) {
            reporter.accept(new DiagnosticImpl(
                this.metadata,
                entity,
                dataRecord.getDataDetails(),
                this.pullRecordValues(dataRecord, this.variables)
            ));
        }

        if (this.target == Target.Record) {
            this.metrics.stop(result > -1, result == 0);
        }

        return result != 0;
    }

    public final boolean validateDataset(SourceDetails entity, Consumer<Diagnostic> reporter) {
        List<Outcome> results = this.performDatasetValidation(entity);

        boolean failed = false;

        for (Outcome resultState : results) {
            if (this.target == Target.Dataset) {
                this.metrics.start();
            }

            byte result = (byte)(resultState.result ^ this.negation);

            if (result == 0) {
                failed = true;

                if (this.isGenerating && reporter != null) {
                    Map<String, String> values = new HashMap<>();

                    for (Map.Entry<String, String> pair : resultState.display.entrySet()) {
                        values.put(pair.getKey(), pair.getValue());
                    }

                    reporter.accept(new DiagnosticImpl(
                        this.metadata,
                        resultState.entity != null ? resultState.entity : entity,
                        null,
                        values
                    ));
                }
            }

            if (this.target == Target.Dataset) {
                this.metrics.stop(result > -1, result == 0);
            }
        }

        return !failed;
    }

    public String getID() {
        return this.metadata.id;
    }

    public void setup(SourceDetails entity) {}

    protected byte performValidation(DataRecord thisRecord) throws CorruptRuleException {
        return -1;
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        return Collections.singletonList(new Outcome((byte)-1));
    }

    protected void addVariable(String variable) {
        this.variables.add(variable.toUpperCase());
    }

    protected Set<String> getVariables() {
        return this.variables;
    }

    protected Map<String, String> pullRecordValues(DataRecord record, Set<String> variables) {
        if (this.includes != null) {
            variables = Sets.union(variables, this.includes);
        }

        Map<String, String> values = new LinkedHashMap<>(variables.size());

        for (String variable : Sets.intersection(record.getVariables(), variables)) {
            if (this.excludes != null && this.excludes.contains(variable)) {
                continue;
            }

            String value = null;

            if (!record.isTransient(variable)) {
                DataEntry entry = record.getValue(variable);

                if (entry.hasValue()) {
                    value = entry.toString();
                }

                values.put(variable, value);
            }
        }

        return values;
    }

    protected static class Outcome {
        final Map<String, String> display = new LinkedHashMap<>();
        final byte result;
        final SourceDetails entity;

        protected Outcome(byte result) {
            this(result, null);
        }

        protected Outcome(byte result, SourceDetails entity) {
            this.result = result;
            this.entity = entity;
        }
    }

    private static class UnqualifiedReferenceDataRecord implements DataRecord {
        private final DataRecord record;

        UnqualifiedReferenceDataRecord(DataRecord record) {
            this.record = record;
        }

        @Override
        public boolean definesVariable(String variable) {
            return this.record.definesVariable(variable)
                || this.record.definesVariable(SubjectDataSupplement.SUBJECT_VARIABLE_PREFIX + variable);
        }

        @Override
        public boolean isTransient(String variable) {
            return this.record.definesVariable(variable)
                ? this.record.isTransient(variable)
                : this.record.isTransient(SubjectDataSupplement.SUBJECT_VARIABLE_PREFIX + variable);
        }

        @Override
        public DataDetails getDataDetails() {
            return this.record.getDataDetails();
        }

        @Override
        public SourceDetails getSourceDetails() {
            return this.record.getSourceDetails();
        }

        @Override
        public DataEntry getValue(String variable) {
            return this.record.definesVariable(variable)
                ? this.record.getValue(variable)
                : this.record.getValue(SubjectDataSupplement.SUBJECT_VARIABLE_PREFIX + variable);
        }

        @Override
        public Set<String> getVariables() {
            return this.record.getVariables();
        }

        @Override
        public int getID() {
            return this.record.getID();
        }
    }
}
