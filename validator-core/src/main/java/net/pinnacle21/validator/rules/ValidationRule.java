/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.report.RuleMetadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author Tim Stone
 */
public interface ValidationRule {
    /**
     *
     * @author Tim Stone
     */
    @Retention(RetentionPolicy.RUNTIME)
    @java.lang.annotation.Target({ ElementType.TYPE })
    @interface RuleAssociation {
        String value();
    }

    /**
     *
     * @author Tim Stone
     */
    enum Target {
        Record,
        Dataset
    }

    String getID();
    Target getTarget();
    RuleMetadata getMetadata();
    void setup(SourceDetails entity);

    default boolean validate(DataRecord dataRecord) throws CorruptRuleException {
        return this.validate(dataRecord, null);
    }

    boolean validate(DataRecord dataRecord, Consumer<Diagnostic> consumer) throws CorruptRuleException;

    default boolean validateDataset(SourceDetails entity) {
        return this.validateDataset(entity, null);
    }

    boolean validateDataset(SourceDetails entity, Consumer<Diagnostic> consumer);
}
