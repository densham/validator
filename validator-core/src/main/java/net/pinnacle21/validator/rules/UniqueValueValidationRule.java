/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.util.DataGrouping;
import net.pinnacle21.validator.ValidationSession;

import java.util.*;

/**
 * @author Tim Stone
 */
public class UniqueValueValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {"Variable"};

    private final Map<DataEntry, DataEntry> interns = new HashMap<>();
    private final Map<DataGrouping, DataGrouping> groupings = new HashMap<>();
    private final boolean matching;
    private final String variable;
    private final String[] groups;

    public UniqueValueValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        this.matching = definition.getProperty("Matching").equalsIgnoreCase("Yes");

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        this.variable = definition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);

        if (definition.hasProperty("GroupBy")) {
            String[] groups = definition.getProperty("GroupBy").split(",");
            this.groups = new String[groups.length];

            for (int i = 0; i < groups.length; ++i) {
                this.groups[i] = groups[i].trim().toUpperCase();

                super.addVariable(this.groups[i]);
            }
        } else {
            this.groups = new String[0];
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        int count = this.groups.length;
        DataEntry[] group = new DataEntry[Math.max(count, 1)];
        DataEntry value = this.intern(dataRecord.getValue(this.variable));

        if (count != 0) {
            for (int i = 0; i < count; ++i) {
                group[i] = this.intern(dataRecord.getValue(this.groups[i]));
            }
        } else {
            group[0] = value;
        }

        DataGrouping search = new DataGrouping(group);
        DataGrouping result = this.groupings.get(search);

        if (result == null && (!this.matching || count > 0 || this.groupings.size() == 0)) {
            if (this.matching) {
                result = new MatchingDataGrouping(search);
            } else {
                result = new UniqueDataGrouping(search);
            }

            this.groupings.put(result, result);
        }

        return (byte)(result != null && result.accepts(value) ? 1 : 0);
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        this.interns.clear();
        this.groupings.clear();

        return super.performDatasetValidation(entity);
    }

    private DataEntry intern(DataEntry entry) {
        return this.interns.computeIfAbsent(entry, k -> entry);
    }

    private static class UniqueDataGrouping extends DataGrouping {
        private final Set<DataEntry> values = new HashSet<>();

        UniqueDataGrouping(DataGrouping grouping) {
            super(grouping);
        }

        public boolean accepts(DataEntry entry) {
            return this.values.add(entry);
        }
    }

    private static class MatchingDataGrouping extends DataGrouping {
        private DataEntry value;

        MatchingDataGrouping(DataGrouping grouping) {
            super(grouping);
        }

        public boolean accepts(DataEntry entry) {
            if (this.value == null) {
                this.value = entry;

                return true;
            }

            return this.value.equals(entry);
        }
    }
}
