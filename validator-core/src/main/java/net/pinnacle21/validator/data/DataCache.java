/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.util.*;

import com.zaxxer.sparsebits.SparseBitSet;
import net.pinnacle21.validator.rules.expressions.PreparedQuery;

/**
 * @author Tim Stone
 */
public class DataCache implements Lookup {
    private final Map<String, SortedMap<DataEntry, SparseBitSet>> cache = new HashMap<>();
    private final Map<String, Map<String, Set<DataEntry>>> casings = new HashMap<>();
    private final Set<String> ignorable = new HashSet<>();
    private final DataEntryFactory factory;

    private volatile boolean isCorrupt  = false;
    private volatile boolean isComplete = false;

    public DataCache(DataEntryFactory factory, Set<String> variables) {
        this(factory);

        this.init(variables);
    }

    public DataCache(DataEntryFactory factory, Set<String> variables, DataCache existingCache) {
        this(factory);

        variables.removeAll(existingCache.cache.keySet());

        this.init(variables);

        for (String key : existingCache.cache.keySet()) {
            this.ignorable.add(key);
            this.cache.put(key, existingCache.cache.get(key));
            this.casings.put(key, existingCache.casings.get(key));
        }
    }

    private DataCache(DataEntryFactory factory) {
        this.factory = factory;
    }

    private void init(Set<String> variables) {
        for (String variable : variables) {
            this.cache.put(variable, new TreeMap<>());
        }
    }

    synchronized void setCorrupt() {
        this.isCorrupt = true;
    }

    synchronized void setComplete() {
        this.isComplete = true;
    }

    synchronized boolean isCorrupt() {
        return this.isCorrupt;
    }

    synchronized boolean isComplete() {
        return this.isComplete;
    }

    void store(final List<DataRecord> dataRecords) {
        Set<String> variables = this.cache.keySet();

        for (DataRecord dataRecord : dataRecords) {
            for (String variable : variables) {
                if (dataRecord.definesVariable(variable) && !this.ignorable.contains(variable)) {
                    DataEntry entry = dataRecord.getValue(variable);
                    SortedMap<DataEntry, SparseBitSet> values = this.cache.get(variable);
                    SparseBitSet records = values.get(entry);

                    if (records == null) {
                        values.put(entry, records = new SparseBitSet());

                        if (entry.getDataType() == DataEntry.DataType.Text) {
                            String original = entry.toString();
                            String keyed = toCasingKey(original);

                            if (!original.equals(keyed)) {
                                this.casings.computeIfAbsent(variable, k -> new HashMap<>())
                                    .computeIfAbsent(keyed, k -> new HashSet<>())
                                    .add(entry);
                            }
                        }
                    }

                    records.set(dataRecord.getID(), true);
                }
            }
        }
    }

    public boolean contains(String variable) {
        return this.cache.containsKey(variable);
    }

    public boolean contains(Set<String> variables) {
        return this.cache.keySet().containsAll(variables);
    }

    public void release(Set<String> variables) {

    }

    public boolean seek(List<PreparedQuery.Mapping> search,
            List<PreparedQuery.Mapping> where, boolean ignoreWhereFailure) {
        boolean result = true;
        SparseBitSet matches = new SparseBitSet();

        if (where.size() > 0) {
            this.search(matches, where);
            result = !matches.isEmpty();
        }

        if (result) {
            // TODO: Add support for or in search when where clause present
            //    Currently attempting to use an or in search when where is
            //    present will break, because the first lookup will be anded
            //    with the results from the where. We'd have to generate the
            //    search results separately and then intersect them with the
            //    where results, but this could be more memory intensive...
            //    Or it might not be an issue in practice, so we'll need to
            //    test and find out.
            this.search(matches, search);
            result = !matches.isEmpty();
        } else {
            result = ignoreWhereFailure;
        }

        return result;
    }

    private void search(SparseBitSet matches, List<PreparedQuery.Mapping> mappings) {
        int shortCircuitIndex = 0;

        // TODO: Implement operator precedence. It's just go from left to right now
        //   This is good enough for our purposes right now, but also inconsistent.
        //   This manner of determining when to short circuit is a bit kludgy as
        //   well.
        int current = 0;

        for (PreparedQuery.Mapping mapping : mappings) {
            PreparedQuery.Mapping.Operator operator = mapping.getOperator();

            if (operator == PreparedQuery.Mapping.Operator.OR) {
                shortCircuitIndex = current;
            }

            ++current;
        }

        // Traverse the where list again, now that we have the short circuit point
        current = 0;

        for (PreparedQuery.Mapping mapping : mappings) {
            PreparedQuery.Mapping.Operator operator = mapping.getOperator();
            PreparedQuery.Mapping.Comparator comparator = mapping.getComparator();
            DataEntry value = mapping.getValue();
            String variable = mapping.getRemote();
            SparseBitSet currentMatches = null;

            SortedMap<DataEntry, SparseBitSet> cache = this.cache.get(variable);

            if (comparator == PreparedQuery.Mapping.Comparator.EQ) {
                currentMatches = cache.get(value);
            } else if (comparator == PreparedQuery.Mapping.Comparator.EIQ) {
                if (value.getDataType() == DataEntry.DataType.Text) {
                    String keyed = toCasingKey(value.toString());
                    Map<String, Set<DataEntry>> casings = this.casings.get(variable);

                    if (casings != null) {
                        Set<DataEntry> conversions = casings.get(keyed);

                        if (conversions != null) {
                            SparseBitSet target = null;

                            for (DataEntry conversion : conversions) {
                                if (target == null) {
                                    target = new SparseBitSet();
                                }

                                target.or(cache.get(conversion));
                            }

                            if (target != null) {
                                currentMatches = target;
                            }
                        }
                    }

                    if (currentMatches == null) {
                        // If the value came in key-case, it won't have a casings
                        // map to use above. We might do this in cases where it's
                        // not necessary, but it shouldn't be a big deal...
                        // TODO: We have to bring in the DataEntryFactory just for this...can we avoid doing that?
                        currentMatches = cache.get(this.factory.create(keyed));
                    }
                } else {
                    currentMatches = cache.get(value);
                }
            } else if (comparator == PreparedQuery.Mapping.Comparator.NEQ) {
                currentMatches = this.convertMapToSet(cache, value);
            } else if (comparator == PreparedQuery.Mapping.Comparator.LT ||
                    comparator == PreparedQuery.Mapping.Comparator.LTE) {
                currentMatches = this.convertMapToSet(cache.headMap(value), null);

                if (comparator == PreparedQuery.Mapping.Comparator.LTE &&
                        cache.containsKey(value)) {
                    currentMatches.or(cache.get(value));
                }
            } else if (comparator == PreparedQuery.Mapping.Comparator.GT ||
                    comparator == PreparedQuery.Mapping.Comparator.GTE) {
                currentMatches = this.convertMapToSet(
                    cache.tailMap(value),
                    comparator == PreparedQuery.Mapping.Comparator.GT ? value : null
                );
            }

            if (currentMatches == null) {
                currentMatches = new SparseBitSet();
            }

            if ((operator == null && matches.isEmpty()) ||
                    operator == PreparedQuery.Mapping.Operator.OR) {
                matches.or(currentMatches);
            } else {
                matches.and(currentMatches);
            }

            // Check if we can short circuit due to failure
            if (matches.isEmpty() && current >= shortCircuitIndex) {
                break;
            }

            ++current;
        }
    }

    private SparseBitSet convertMapToSet(Map<DataEntry, SparseBitSet> selection, DataEntry excluding) {
        SparseBitSet target = new SparseBitSet();

        for (Map.Entry<DataEntry, SparseBitSet> entry : selection.entrySet()) {
            if (excluding == null || !excluding.equals(entry.getKey())) {
                target.or(entry.getValue());
            }
        }

        return target;
    }

    private String toCasingKey(String key) {
        return key.toUpperCase();
    }
}
