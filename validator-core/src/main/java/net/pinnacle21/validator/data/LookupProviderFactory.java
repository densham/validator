/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LookupProviderFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(LookupProviderFactory.class);

    private final ConcurrentMap<String, LookupProvider> externalProviders = new ConcurrentHashMap<>();
    private final LookupProvider defaultProvider;

    public LookupProviderFactory(LookupProvider defaultProvider) {
        this.defaultProvider = defaultProvider;
    }

    public LookupProvider getProvider(String adapter) {
        if (adapter == null) {
            return this.defaultProvider;
        }

        LookupProvider provider = this.externalProviders.computeIfAbsent(adapter, c -> {
            try {
                Class<?> impl = Class.forName(c);

                if (impl != null) {
                    return (LookupProvider)impl.newInstance();
                }
            } catch (ClassNotFoundException
                    | ClassCastException
                    | InstantiationException
                    | IllegalAccessException ex) {
                LOGGER.error("Unable to create an instance of {} due to an exception", c, ex);
            }

            return null;
        });

        if (provider == null) {
            throw new IllegalArgumentException(String.format("The identifier %s does not match an available "
                    + "LookupProvider implementation", adapter));
        }

        return provider;
    }
}
