/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import static org.junit.Assert.*;

import java.util.List;

import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import org.junit.Test;

/**
 * @author Tim Stone
 */
public class CombinedDataSourceTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void CombinedSourcesAllRecords() throws Exception {
        DataSource lbch = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build(), this.factory);
        DataSource lbhe = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBHE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"))
            .build(), this.factory);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(this.factory, lbch, lbhe);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
    }

    @Test
    public void CombinedSourcesVaryingVariables() throws Exception {
        DataSource lbch = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build(), this.factory);
        DataSource lbte = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBTE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbte.csv"))
            .build(), this.factory);

        assertTrue(lbch.test());
        assertTrue(lbte.test());

        DataSource combined = new CombinedDataSource(this.factory, lbch, lbte);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
        assertTrue(records.get(0).definesVariable("NEW"));
    }

    /**
     * Assertion: Data records from combined data sources have a system-internal variable
     *     added to them
     */
    @Test
    public void CombinedSourcesAugmentRecordsWithDataSet() throws Exception {
        DataSource lbch = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build(), this.factory);
        DataSource lbhe = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBHE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"))
            .build(), this.factory);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(this.factory, lbch, lbhe);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
        assertTrue(records.get(0).definesVariable("VAL:DATASET"));
        assertEquals(this.factory.create("LBCH"), records.get(0).getValue("VAL:DATASET"));
        assertTrue(records.get(2).definesVariable("VAL:DATASET"));
        assertEquals(this.factory.create("LBHE"), records.get(2).getValue("VAL:DATASET"));
    }

    /**
     * Assertion: Combined data sources should represent the metadata of each component
     *     source as part of a combined data source returned by getMetadata().
     */
    @Test
    public void CombinedSourcesHaveCombinedMetadata() throws Exception {
        DataSource lbch = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build(), this.factory);
        DataSource lbhe = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBHE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"))
            .build(), this.factory);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(this.factory, lbch, lbhe);
        DataSource metadata = combined.getMetadata();

        assertTrue(metadata.isComposite());
        assertTrue(metadata.isMetadata());

        List<DataRecord> records = metadata.getRecords();

        assertEquals(10, records.size());
        assertEquals(this.factory.create("LBCH"), records.get(0).getValue("DATASET"));
        assertEquals(this.factory.create("LBHE"), records.get(5).getValue("DATASET"));
    }

    /**
     * Assertion: Replicated combined metadata should be able to be combined with normal
     *     replicated metadata.
     */
    @Test
    public void CombinedSourceMetadataCanCombineWithNormalMetadata() throws Exception {
        CombinedDataSource global = new CombinedDataSource("GLOBAL", "System", this.factory);

        DataSource lbch = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build(), this.factory);
        DataSource lbhe = new DelimitedDataSource(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBHE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"))
            .build(), this.factory);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(this.factory, lbch, lbhe);
        DataSource metadata = combined.getMetadata();

        global.add(lbch.getMetadata().replicate());
        global.add(metadata.replicate());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
