/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.*;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.SourceDetails;

/**
 * Provides the essential common functionality for a <code>DataSource</code> to operate
 * reliably. Implementations of <code>DataSource</code> are strongly suggested to
 * subclass <code>AbstractDataSource</code> to keep the logic behind storing variables
 * and setting statuses consistent.
 *
 * @author Tim Stone
 */
public abstract class AbstractDataSource implements DataSource {
    static final int DEFAULT_RECORD_COUNT = 10;

    private boolean isFinished = false;
    private int recordCount = 0;
    private Set<String> variables = null;

    protected InternalEntityDetails entity;
    protected final SourceOptions options;
    protected final Metadata metadata;
    protected final DataEntryFactory factory;
    protected final Charset charset;

    protected AbstractDataSource(SourceOptions options, DataEntryFactory factory) {
        this(options, factory, null);
    }

    protected AbstractDataSource(SourceOptions options, DataEntryFactory factory, Charset defaultCharset) {
        this.options = options;
        this.factory = factory;
        this.entity = new InternalEntityDetails(
            SourceDetails.Reference.Data, options.getName(), options.getSubname(), options.getSource()
        );
        this.entity.setProperty(SourceDetails.Property.Records, 0);
        this.metadata = new Metadata(this.entity, this.factory);
        this.charset = Optional.ofNullable(options.getCharset()).orElse(defaultCharset);
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(SourceDetails.Property.Location);
    }

    public DataSource getMetadata() {
        return this.metadata;
    }

    public String getName() {
        return this.entity.getString(SourceDetails.Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() throws InvalidDataException {
        return this.getRecords(DEFAULT_RECORD_COUNT);
    }

    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException {
        if (this.variables == null) {
            this.getVariables();
        }

        try {
            return this.parseRecords(recordCount);
        } catch (InvalidDataException ex) {
            this.entity.setProperty(SourceDetails.Property.Corrupted, true);

            throw ex;
        }
    }

    public synchronized Set<String> getVariables() throws InvalidDataException {
        if (this.variables == null) {
            Set<String> variables = new LinkedHashSet<>();
            boolean threw = false;

            try {
                this.parseVariables();

                if (this.metadata.hasRecords()) {
                    for (DataRecord record : this.metadata.getRecords()) {
                        String variable = record.getValue("VARIABLE").toString();

                        if (variables.contains(variable)) {
                            throw new InvalidDataException(InvalidDataException.Codes.DuplicateVariable,
                                    String.format("The variable %s must only appear once", variable));
                        }

                        variables.add(variable);
                    }

                    this.metadata.reset();
                }
            } catch (InvalidDataException ex) {
                threw = true;

                throw ex;
            } finally {
                this.variables = Collections.unmodifiableSet(variables);

                if (threw || this.variables.isEmpty()) {
                    this.entity.setProperty(SourceDetails.Property.Corrupted, true);
                }
            }
        }

        if (this.variables.isEmpty()) {
            throw new InvalidDataException(InvalidDataException.Codes.NoVariables,
                    "This source does not appear to contain variable definitions.");
        }

        return this.variables;
    }

    public Object getVariableProperty(String variable, VariableProperty property)
            throws InvalidDataException {
        if (this.variables == null) {
            this.getVariables();
        }

        DataRecord record = this.metadata.getVariable(variable);

        if (record == null) {
            throw new IllegalArgumentException(String.format(
                "The variable %s is not defined for this source", variable
            ));
        }

        // TODO: Why aren't we getting VariableDetails here instead?
        Object value = record.getValue(property.toString().toUpperCase()).getValue();

        // Backwards compatibility
        return value instanceof BigDecimal ? ((BigDecimal)value).doubleValue() : value;
    }

    public boolean hasRecords() {
        return !this.isFinished && !this.entity.getBoolean(SourceDetails.Property.Corrupted, false);
    }

    public boolean isComposite() {
        return false;
    }

    public boolean isMetadata() {
        return false;
    }

    protected abstract List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException;

    protected abstract void parseVariables() throws InvalidDataException;

    protected void markComplete() {
        this.isFinished = true;
        this.close();
    }

    protected DataRecord newRecord(ImmutableMap<String, DataEntry> values) {
        return new DataRecordImpl(
            new InternalDataDetails(this.getRecordCount(), DataDetails.Info.Data),
            this.entity,
            values
        );
    }

    protected void next() {
        ++this.recordCount;

        this.entity.setProperty(SourceDetails.Property.Records, this.recordCount);
    }

    protected void rewind(int offset) {
        this.recordCount -= offset;

        this.entity.setProperty(SourceDetails.Property.Records, this.recordCount);
    }
}
