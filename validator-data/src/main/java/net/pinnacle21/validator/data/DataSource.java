/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.io.Closeable;
import java.util.List;
import java.util.Set;

/**
 * Produces <code>DataRecord</code>s for validation by a <code>Validator</code> instance.
 * A <code>DataSource</code> may retrieve data from any form of storage, though it is
 * expected that the data will remain constant throughout the reading process.
 *
 * @author Tim Stone
 */
public interface DataSource extends Closeable {
    /**
     *
     *
     * @author Tim Stone
     */
    public enum VariableProperty {
        Name,
        Type,
        Length,
        Label,
        Order,
        Format
    }

    /**
     * Closes any resources this <code>DataSource</code> has open.
     */
    public void close();

    /**
     *
     * @return
     */
    public InternalEntityDetails getDetails();

    /**
     *
     * @return
     */
    public String getLocation();

    /**
     *
     * @return
     */
    public DataSource getMetadata();

    /**
     *
     *
     * @return the name of the <code>DataSource</code>
     */
    public String getName();

    /**
     * Gets the number of records that have already been read by this source.
     *
     * @return the current record count
     */
    public int getRecordCount();

    /**
     * Reads data by calling {@link #getRecords(int)} with the implementation-specific
     * default record count.
     *
     * @return a <code>List</code> of <code>DataRow</code> objects representing the data
     * @throws InvalidDataException  if there is no more data to read, or if another
     *     if another error occurred while trying to read
     * @see #getRecords(int)
     */
    public List<DataRecord> getRecords() throws InvalidDataException;

    /**
     * Reads data from an implementation-specific input and returns it, in order, in the
     * form of <code>DataRecord</code> objects. The number of items returned is either
     * <code>recordCount</code> or the remaining records that can be read from the
     * source, whichever is less.
     * <p>
     * This method can only be called while {@link #hasRecords()} returns
     * <code>true</code>.
     *
     * @param recordCount  the number of records to request on this read attempt
     * @return a <code>List</code> of <code>DataRow</code> objects representing the data
     * @throws InvalidDataException  if there is no more data to read, or if another
     *     error occurred while trying to read
     * @see #hasRecords()
     */
    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException;

    /**
     * Produces a <code>List</code> of variable names that are defined in a given
     * <code>DataSource</code>.
     *
     * @return a <code>List</code> of unique variable names
     * @throws InvalidDataException  if there is not any variable information to read, or
     *     if another error occurred while trying to read
     */
    public Set<String> getVariables() throws InvalidDataException;

    /**
     * Gets the value of the given property of the specified variable, provided that the
     * variable exists in the <code>DataSource</code>. If the <code>DataSource</code> is
     * of a type that does not store information about a property (i.e. CSV files do not
     * have labels), then <code>null</code> will be returned.
     *
     * @param variable  the name of the variable
     * @param property  the property to get the value of
     * @throws IllegalArgumentException  if the specified <code>variable</code> isn't
     *     present in the data
     * @throws InvalidDataException  if calling this method required metadata to be
     *     read, and that process failed
     * @return the value of the property, or <code>null</code> if the property isn't
     *     defined by the source data
     */
    public Object getVariableProperty(String variable, VariableProperty property)
            throws InvalidDataException;

    /**
     * Determines whether or not there are more <code>DataRecord</code> objects to be
     * read.
     *
     * @return <code>true</code> if there is more data available, <code>false</code>
     *     otherwise
     */
    public boolean hasRecords();

    /**
     * Determines if a given <code>DataSource</code> is composite. Composite data sources
     * are unified views of multiple sources grouped under a common name.
     *
     * @return <code>true</code> if the data returned is composite, <code>false</code>
     *     otherwise
     */
    public boolean isComposite();

    /**
     * Determines if a given <code>DataSource</code> represents the metadata of another
     * source.
     *
     * @return <code>true</code> if the data returned is metadata, <code>false</code>
     *     otherwise
     */
    public boolean isMetadata();

    /**
     * Creates a copy of a given <code>DataSource</code>, producing a new instance that
     * can operate on the source data in a way that won't impact future reads from the
     * original.
     *
     * @return an independent <code>DataSource</code> reference
     * @throws InvalidDataException  if an error occurs when creating the independent
     *     <code>DataSource</code> instance
     */
    public DataSource replicate() throws InvalidDataException;

    /**
     * Attempts to determine with a reasonable level of accuracy that the data for a
     * <code>DataSource</code> can be parsed in the manner expected. This helps to
     * identify any potential issues that might come from using this source up front,
     * instead of waiting for attempts to read to fail later in the flow.
     *
     * @return <code>true</code> if it appears that reading from this source will be
     *     successful, otherwise <code>false</code>
     */
    public boolean test();
}
