/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.SourceDetails;

/**
 * @author Tim Stone
 */
public class DataRecordImpl extends BaseDataRecord {
    private final InternalEntityDetails entity;
    private final DataDetails record;

    public DataRecordImpl(DataDetails record, InternalEntityDetails entity, ImmutableMap<String, DataEntry> values) {
        super(values);

        this.record = record;
        this.entity = entity;
    }



    public boolean isTransient(String variable) {
        return false;
    }

    public DataDetails getDataDetails() {
        return this.record;
    }

    public SourceDetails getSourceDetails() {
        return this.entity;
    }

    public int getID() {
        return this.record.getId();
    }
}
