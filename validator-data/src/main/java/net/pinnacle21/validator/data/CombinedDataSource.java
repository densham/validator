/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.SourceDetails;

/**
 *
 * @author Tim Stone
 */
public class CombinedDataSource implements DataSource {
    private static final String SPLIT_VARIABLE = "VAL:DATASET";

    private final Set<String> variables = new LinkedHashSet<>();
    private final boolean identifyAsMetadata;
    private final List<DataSource> sources = new ArrayList<>();
    private final CombinedDataSource metadata;
    private final InternalEntityDetails entity;
    private final DataEntryFactory factory;

    private AugmentMetadata augment = null;
    private boolean isMetadata = false;
    private int recordCount = 0;
    private DataSource source = null;
    private int sourcePosition = 0;

    public CombinedDataSource(DataEntryFactory factory, DataSource... sources) {
        this(
            sources.length > 0 ? sources[0].getDetails().getString(SourceDetails.Property.Name) : null,
            combineLocations(sources),
            factory
        );

        for (DataSource source : sources) {
            this.add(source);
        }
    }

    public CombinedDataSource(String name, String location, DataEntryFactory factory) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        this.entity = new InternalEntityDetails(SourceDetails.Reference.Data, name, location);
        this.entity.setProperty(SourceDetails.Property.Subname, name);
        this.entity.setProperty(SourceDetails.Property.Combined, true);
        this.metadata = new CombinedDataSource(this.entity, factory);
        this.identifyAsMetadata = false;
        this.factory = factory;
    }

    private CombinedDataSource(InternalEntityDetails entity, DataEntryFactory factory) {
        this.entity = new InternalEntityDetails(SourceDetails.Reference.Metadata, entity);
        this.metadata = null;
        this.isMetadata = true;
        this.identifyAsMetadata = true;
        this.factory = factory;
    }

    public List<DataSource> getSources() {
        return this.sources;
    }

    public void add(DataSource source) {
        try {
            if (this.sources.isEmpty()) {
                this.isMetadata = source.isMetadata();
                this.source = source;
                this.confirmAndUpdateMetadata(source);
            } else if (!this.confirmAndUpdateMetadata(source)) {
                throw new IllegalArgumentException("sources had incompatible metadata");
            }
        } catch (InvalidDataException ex) {
            throw new IllegalArgumentException("invalid source");
        }

        this.sources.add(source);
        this.entity.addSubentity(source.getDetails());
        this.entity.setProperty(SourceDetails.Property.Location, combineLocations(
            this.sources.toArray(new DataSource[0]))
        );
    }

    public void close() {
        for (DataSource source : this.sources) {
            source.close();
        }
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(SourceDetails.Property.Location);
    }

    public DataSource getMetadata() {
        return this.metadata;
    }

    public String getName() {
        return this.entity.getString(SourceDetails.Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() throws InvalidDataException {
        return this.getRecords(AbstractDataSource.DEFAULT_RECORD_COUNT);
    }

    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException {
        List<DataRecord> records = new ArrayList<>();

        if (this.augment == null) {
            this.determineAugment();
        }

        while (this.hasRecords() && records.size() < recordCount) {
            if (this.source.hasRecords()) {
                records.addAll(this.augment(this.source.getRecords(recordCount)));
            }

            if (records.size() < recordCount || !this.source.hasRecords()) {
                ++this.sourcePosition;

                if (this.sourcePosition < this.sources.size()) {
                    this.source = this.sources.get(this.sourcePosition);
                    this.determineAugment();
                }
            }
        }

        this.entity.setProperty(SourceDetails.Property.Records, this.recordCount);

        return records;
    }

    public Set<String> getVariables() {
        // TODO: Are we supposed to be guaranteeing order here?
        //   If so, the interface should actually enforce that
        return new LinkedHashSet<>(
            this.isMetadata ? Metadata.VARIABLES : this.variables
        );
    }

    public Object getVariableProperty(String variable, VariableProperty property) {
        throw new UnsupportedOperationException(
            "cannot call getVariableProperty on a combined data source"
        );
    }

    public boolean hasRecords() {
        return this.source != null && this.source.hasRecords();
    }

    public boolean isComposite() {
        return true;
    }

    public boolean isMetadata() {
        return this.identifyAsMetadata && this.isMetadata;
    }

    public DataSource replicate() throws InvalidDataException {
        CombinedDataSource duplicate;

        // TODO: This is not the ideal way to replicate this source
        if (!this.identifyAsMetadata) {
            duplicate = new CombinedDataSource(
                this.entity.getString(SourceDetails.Property.Name),
                this.entity.getString(SourceDetails.Property.Location),
                this.factory
            );
        } else {
            duplicate = new CombinedDataSource(this.entity, this.factory);
        }

        for (DataSource source : this.sources) {
            duplicate.add(source.replicate());
        }

        return duplicate;
    }

    public boolean test() {
        // All of the individual sources should have been tested by now, so we can just
        // say that the test succeeds (I don't think this will ever be called, actually)
        return true;
    }

    private List<DataRecord> augment(List<DataRecord> records) {
        int originalCount = this.recordCount;
        this.recordCount += records.size();

        if (this.isMetadata) {
            return records;
        }

        ImmutableMap.Builder<String, DataEntry> builder = ImmutableMap.<String, DataEntry>builder()
            .put(SPLIT_VARIABLE, this.augment.splitVariable);

        for (String variable : this.augment.variables) {
            builder.put(variable, DataEntry.NULL_ENTRY);
        }

        ImmutableMap<String, DataEntry> values = builder.build();

        return IntStream.range(0, records.size())
            .mapToObj(i -> new SupplementalRenumberedDataRecord(records.get(i), values, (i + 1) + originalCount))
            .collect(Collectors.toList());
    }

    private void determineAugment() {
        if (this.isMetadata) {
            return;
        }

        try {
            DataEntry splitVariable = this.factory.create(this.source.getDetails()
                    .getString(SourceDetails.Property.Subname));

            Set<String> defined = this.source.getVariables();
            Set<String> combined = new LinkedHashSet<>(this.variables);

            combined.removeAll(defined);

            this.augment = new AugmentMetadata(combined, splitVariable);
        } catch (InvalidDataException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean confirmAndUpdateMetadata(DataSource source)
            throws InvalidDataException {
        boolean matches = true;

        if (source.isMetadata() != this.isMetadata) {
            throw new IllegalArgumentException("The two sources are not the same "
                    + "reference type");
        }

        if (this.isMetadata) {
            matches = source.getVariables().equals(Metadata.VARIABLES);
        } else {
            this.variables.addAll(source.getVariables());
            this.metadata.add(source.getMetadata().replicate());
        }

        return matches;
    }

    private static String combineLocations(DataSource[] sources) {
        StringBuilder builder = new StringBuilder();

        if (sources.length == 0) {
            return "unknown";
        }

        for (int i = 0; i < sources.length; ++i) {
            if (i > 0) {
                builder.append(File.pathSeparatorChar);
            }

            builder.append(sources[i].getLocation());
        }

        return builder.toString();
    }

    private static class SupplementalRenumberedDataRecord extends SupplementalDataRecord {
        private final int id;

        SupplementalRenumberedDataRecord(DataRecord record, ImmutableMap<String, DataEntry> values, int id) {
            super(record, values, false);

            this.id = id;
        }

        @Override
        public int getID() {
            return this.id;
        }
    }

    private static class AugmentMetadata {
        final Set<String> variables;
        final DataEntry splitVariable;

        AugmentMetadata(Set<String> variables, DataEntry splitVariable) {
            this.variables = variables;
            this.splitVariable = splitVariable;
        }
    }
}
