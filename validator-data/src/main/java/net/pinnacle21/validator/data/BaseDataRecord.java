/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;

import java.util.Set;

/**
 * @author Tim Stone
 */
abstract class BaseDataRecord implements DataRecord {
    protected final ImmutableMap<String, DataEntry> values;

    protected BaseDataRecord(ImmutableMap<String, DataEntry> values) {
        this.values = values;
    }

    @Override
    public boolean definesVariable(String variable) {
        return this.values.containsKey(variable);
    }

    @Override
    public DataEntry getValue(String variable) {
        DataEntry value = this.values.get(variable);

        if (value == null) {
            throw new IllegalArgumentException(String.format(
                "The variable %s does not exist in this DataRecord",
                variable
            ));
        }

        return value;
    }

    @Override
    public Set<String> getVariables() {
        return this.values.keySet();
    }
}
