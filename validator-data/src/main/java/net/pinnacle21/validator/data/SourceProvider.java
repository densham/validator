/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.SourceOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toMap;

/**
 *
 * @author Tim Stone
 */
public class SourceProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceProvider.class);
    private static final Map<SourceOptions.Type, Class<? extends DataSource>> SOURCE_TYPES = ImmutableMap
        .<SourceOptions.Type, Class<? extends DataSource>>builder()
        .put(SourceOptions.StandardTypes.DatasetXml, DatasetXmlDataSource.class)
        .put(SourceOptions.StandardTypes.SasTransport, SasTransportDataSource.class)
        .put(SourceOptions.StandardTypes.Delimited, DelimitedDataSource.class)
        .build();

    private final Map<String, DataSource> sources = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    private final DataEntryFactory factory;

    public SourceProvider(DataEntryFactory factory) {
        this.factory = factory;
    }

    public void add(SourceOptions options) {
        DataSource createdSource = this.tryCreateSource(options);

        if (createdSource == null || !createdSource.test()) {
            // TODO: Create dummy source
            return;
        }

        DataSource existingSource = this.sources.get(options.getName());

        if (existingSource != null) {
            CombinedDataSource combinedSource;

            if (!existingSource.isComposite()) {
                existingSource.getDetails().setProperty(SourceDetails.Property.Split, true);
                combinedSource = new CombinedDataSource(this.factory, existingSource);

                // Replace the existing normal source with the new composite once
                this.sources.put(options.getName(), combinedSource);
            } else {
                if (!(existingSource instanceof CombinedDataSource)) {
                    throw new RuntimeException(String.format(
                        "existing source with name %s was not a CombinedDataSource as expected", options.getName()
                    ));
                }

                combinedSource = (CombinedDataSource)existingSource;
            }

            createdSource.getDetails().setProperty(SourceDetails.Property.Split, true);
            combinedSource.add(createdSource);
        } else {
            this.sources.put(options.getName(), createdSource);
        }
    }

    public void add(List<SourceOptions> sources) {
        for (SourceOptions source : sources) {
            this.add(source);
        }
    }

    public boolean containsSource(String sourceName) {
        return this.sources.containsKey(sourceName);
    }

    public boolean containsValidSource(String sourceName) {
        return this.containsSource(sourceName);
    }

    public DataSource getSource(String sourceName) {
        return this.getSource(sourceName, false);
    }

    public DataSource getSource(String sourceName, boolean replicated) {
        if (!this.sources.containsKey(sourceName)) {
            throw new IllegalArgumentException(String.format("Unknown source %s", sourceName));
        }

        DataSource source = this.sources.get(sourceName);

        if (source != null && replicated) {
            try {
                source = source.replicate();
            } catch (InvalidDataException ex) {}
        }

        return source;
    }

    public Set<String> getSourceNames() {
        return this.sources.keySet();
    }

    // TODO: Method should maybe be on SourceOptions itself but it requires some knowledge of what sources we can create?
    public SourceOptions parseSource(String connectionString) {
        SourceOptions.Builder builder = null;

        boolean headerless = false;
        boolean unquoted = false;
        boolean isLegacy = false;
        String type = null;

        // TODO: Remove legacy support
        if (connectionString.startsWith("FILE:")) {
            String[] pieces = connectionString.split(":", 3);

            if (pieces.length == 3) {
                isLegacy = true;

                String typeName = pieces[1];
                String location = pieces[2];

                builder = SourceOptions.builder()
                        .withSource(location);

                pieces = typeName.split("-");
                type = pieces[0];

                for (int i = 1; i < pieces.length; ++i) {
                    if ("HEADERLESS".equalsIgnoreCase(pieces[i])) {
                        headerless = true;
                    } else if ("UNQUOTED".equalsIgnoreCase(pieces[i])) {
                        unquoted = true;
                    }
                }
            }
        }

        if (!isLegacy) {
            try {
                URI uri = new URI(connectionString);
                Map<String, String> params = parseSimpleQueryString(uri);

                builder = SourceOptions.builder()
                        .withSource(uri.getPath());

                type = params.remove("type");

                if (type == null && "file".equalsIgnoreCase(uri.getScheme())) {
                    String fileName = new File(uri.getPath()).getName();
                    int index = fileName.indexOf('.');

                    if (index != -1) {
                        type = fileName.substring(index + 1);
                    }
                }

                headerless = params.remove("headerless") != null;
                unquoted = params.remove("unquoted") != null;
            } catch (URISyntaxException ex) {
                LOGGER.error("Unable to parse {} as a valid URI to create source options", connectionString);
            }
        }

        if (builder == null) {
            return null;
        }

        final String chosenType = type;

        SourceOptions.withType(type, builder).orElseThrow(() ->
                new IllegalArgumentException(String.format("%s has unknown source type %s", connectionString, chosenType)));

        if (headerless) {
            builder.withProperty("Header", "false");
        }

        if (unquoted) {
            builder.withProperty("Qualifier", "\0");
        }

        return builder.withName(connectionString)
                .build();
    }

    private DataSource tryCreateSource(SourceOptions options) {
        Class<? extends DataSource> impl = SOURCE_TYPES.get(options.getType());

        if (impl == null) {
            // TODO: Logging
            return null;
        }

        try {
            return impl.getDeclaredConstructor(SourceOptions.class, DataEntryFactory.class)
                .newInstance(options, this.factory);
        } catch (NoSuchMethodException
                | InstantiationException
                | IllegalAccessException
                | InvocationTargetException ex) {
            LOGGER.error("Unable to create source due to exception", ex);
        }

        return null;
    }

    // This isn't a universally correct method of doing this but our use case is pretty limited
    private static Map<String, String> parseSimpleQueryString(URI uri) {
        return Pattern.compile("&")
            .splitAsStream(uri.getQuery())
            .map(s -> Arrays.copyOf(s.split("=", 2), 2))
            .collect(toMap(s -> decode(s[0]).toLowerCase(), s -> decode(s[1])));
    }

    private static String decode(String part) {
        try {
            return part == null
                ? ""
                : URLDecoder.decode(part, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException("Can't even happen, sigh", ex);
        }
    }
}
