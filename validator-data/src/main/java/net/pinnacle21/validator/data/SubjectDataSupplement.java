/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;

import java.util.*;

/**
 * @author Tim Stone
 */
public class SubjectDataSupplement implements DataSupplement {
    public static final String SUBJECT_VARIABLE_PREFIX = "SUB:";

    // TODO: This is sort of experimental at the moment, so the key value is hard-coded
    //    Ideally we can support this via external configuration, like some attribute on
    //    the ItemGroupDef in the configuration file.
    private static final String KEY_VARIABLE = "USUBJID";
    private static final String ALT_KEY_VARIABLE = "POOLID";
    private final Set<String> variables = new LinkedHashSet<>();
    private final Map<DataEntry, DataEntry> interns = new HashMap<>();
    private final Map<DataEntry, DataEntry[]> groupings = new HashMap<>();

    private final String keyVariable;

    public SubjectDataSupplement(DataSource originalSource) throws InvalidDataException {
        try (DataSource source = originalSource.replicate()) {
            Set<String> variables = new HashSet<>(source.getVariables());

            if (variables.contains(KEY_VARIABLE)) {
                this.keyVariable = KEY_VARIABLE;
            } else if (variables.contains(ALT_KEY_VARIABLE)) {
                this.keyVariable = ALT_KEY_VARIABLE;
            } else {
                this.keyVariable = null;

                return;
            }

            int count = variables.size();

            while (source.hasRecords()) {
                for (DataRecord record : source.getRecords()) {
                    DataEntry key = this.intern(record.getValue(this.keyVariable));
                    DataEntry[] values = new DataEntry[count];

                    int i = 0;

                    for (String variable : variables) {
                        values[i++] = this.intern(
                            record.getValue(variable)
                        );
                    }

                    this.groupings.put(key, values);
                }
            }

            for (String variable : variables) {
                this.variables.add(SUBJECT_VARIABLE_PREFIX + variable);
            }
        }
    }

    public DataRecord augment(DataRecord record) {
        // TODO: Technically we should defer augmentation until it's actually needed
        //    We need to see if there's any significant performance implications
        //    that come with doing this regardless of need
        // TODO: We should probably pull this check to a higher level
        //    The calling code should know not to try and augment sources that it
        //    has no reason to be attempting to augment, most likely
        if (this.keyVariable != null && record.definesVariable(this.keyVariable)) {
            DataEntry key = record.getValue(this.keyVariable);
            DataEntry[] values = this.groupings.get(key);

            int count = 0;
            ImmutableMap.Builder<String, DataEntry> supplementalValues = ImmutableMap.builder();

            for (String variable : this.variables) {
                supplementalValues.put(variable, values != null ? values[count] : DataEntry.NULL_ENTRY);

                count++;
            }

            if (count > 0) {
                return new SupplementalDataRecord(record, supplementalValues.build(), false);
            }
        }

        return record;
    }

    // TODO: I'm not sure why we'd get any benefit from interning here?
    private DataEntry intern(DataEntry entry) {
        return this.interns.computeIfAbsent(entry, k -> entry);
    }
}
