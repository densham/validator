/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.SourceDetails;

import java.util.Set;

/**
 * Contains a single "row" of data from a given <code>DataSource</code>. A row is
 * comprised of <code>DataEntry</code> objects index by their given variable name.
 *
 * @author Tim Stone
 */
public interface DataRecord {
    /**
     * @param variable  the name of the variable to check for
     * @return <code>true</code> if the variable is defined for this record, <code>false</code> otherwise
     */
    boolean definesVariable(String variable);

    boolean isTransient(String variable);

    /**
     * @return the <code>DataDetails</code> identifying this record
     */
    DataDetails getDataDetails();

    /**
     * @return the <code>EntityDetails</code> representing the source of this record
     */
    SourceDetails getSourceDetails();

    /**
     * @param variable  the name of the variable to retrieve
     * @return the <code>DataEntry</code> assigned to the provided variable
     * @throws IllegalArgumentException  if the variable is not defined for this record
     */
    DataEntry getValue(String variable);

    /**
     * @return the <code>Set</code> of variables defined for this record
     */
    Set<String> getVariables();

    /**
     * @return the per-source ID of this record
     */
    int getID();
}
