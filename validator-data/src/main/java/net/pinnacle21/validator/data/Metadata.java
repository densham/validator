/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.util.*;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.SourceDetails;

/**
 *
 * @author Tim Stone
 */
class Metadata implements DataSource {
    static final Set<String> VARIABLES = new LinkedHashSet<>(Arrays.asList(
        "DOMAIN",
        "DATASET",
        "VARIABLE",
        "TYPE",
        "LENGTH",
        "LABEL",
        "ORDER",
        "FORMAT"
    ));

    private final InternalEntityDetails entity;
    private final InternalEntityDetails parent;
    private int recordCount = 0;
    private final List<DataRecord> records = new ArrayList<>();
    private final Map<String, Integer> locations = new HashMap<>();
    private final DataEntryFactory factory;

    Metadata(InternalEntityDetails entity, DataEntryFactory factory) {
        this(null, entity, factory);
    }

    private Metadata(InternalEntityDetails parent, InternalEntityDetails entity, DataEntryFactory factory) {
        this.reset();
        this.parent = parent != null ? parent : entity;
        this.entity = new InternalEntityDetails(SourceDetails.Reference.Metadata, entity, this.parent);
        this.factory = factory;
    }

    public void close() {
        // Do nothing
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(SourceDetails.Property.Location);
    }

    public DataSource getMetadata() {
        throw new UnsupportedOperationException("cannot get metadata from metadata");
    }

    public String getName() {
        return this.entity.getString(SourceDetails.Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() {
        return this.getRecords(this.records.size());
    }

    public List<DataRecord> getRecords(int recordCount) {
        List<DataRecord> records = new ArrayList<>();
        int current = this.recordCount;
        int bound = Math.min(current + recordCount, this.records.size());

        for ( ; current < bound; ++current) {
            records.add(this.records.get(current));
        }

        this.recordCount = current;

        return records;
    }

    public Set<String> getVariables() {
        return VARIABLES;
    }

    public Object getVariableProperty(String variable, VariableProperty property) {
        throw new UnsupportedOperationException(
            "Can't get variable properties of a metadata source"
        );
    }

    public boolean hasRecords() {
        return this.recordCount < this.records.size();
    }

    public boolean isComposite() {
        return false;
    }

    public boolean isMetadata() {
        return true;
    }

    public DataSource replicate() {
        Metadata duplicate = new Metadata(this.parent, this.entity, this.factory);

        // Transfer the records
        duplicate.records.addAll(this.records);

        return duplicate;
    }

    public boolean test() {
        return true;
    }

    DataRecord getVariable(String name) {
        DataRecord variable = null;
        name = name.toUpperCase();

        if (this.locations.containsKey(name)) {
            variable = this.records.get(this.locations.get(name));
        }

        return variable;
    }

    Set<String> getVariableNames() {
        return this.locations.keySet();
    }

    void add(String variable) {
        this.add(variable, null, null, null, null);
    }

    void add(String variable, String type, Integer length, String label, String format) {
        add(variable, type, length, label, format, format);
    }

    void add(String variable, String type, Integer length, String label, String format, String fullFormat) {
        int order = this.records.size() + 1;
        String domain = this.entity.getString(SourceDetails.Property.Name).toUpperCase();
        String dataset = null;
        variable = variable.toUpperCase();

        if (this.entity.hasProperty(SourceDetails.Property.Subname)) {
            dataset = this.entity.getString(SourceDetails.Property.Subname);
        }

        DataRecord record = new DataRecordImpl(
            new InternalDataDetails(order, variable, DataDetails.Info.Variable),
            this.entity,
            ImmutableMap.<String, DataEntry>builder()
                .put("DOMAIN", this.factory.create(domain))
                .put("DATASET", this.factory.create(dataset))
                .put("VARIABLE", this.factory.create(variable))
                .put("TYPE", this.factory.create(type))
                .put("LENGTH", this.factory.create(length))
                .put("LABEL", this.factory.create(label))
                .put("ORDER", this.factory.create((double)order))
                .put("FORMAT", this.factory.create(format))
                .build()
        );

        this.records.add(record);
        this.locations.put(variable.toUpperCase(), this.records.size() - 1);
        this.entity.setProperty(SourceDetails.Property.Variables, this.records.size());
        this.parent.addVariable(new InternalVariableDetails(
            variable, order, type, length, label, format, fullFormat
        ));
    }

    /**
     *
     */
    void reset() {
        this.recordCount = 0;
    }
}
