/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

public class Values {
    /**
     * Rounds a double value to a given number of significant digits past the decimal
     * place. This is done to generate the "right" numbers, due to conversion issues
     * that occur from different number storage formats and the frailness of floating
     * point arithmetic.
     * <p>
     * <em>Adapted from http://stackoverflow.com/a/1581007</em>
     *
     * @param input  the value to round off
     * @param cutoff  the precision to round to
     * @return the corrected number
     */
    public static double round(double input, int cutoff) {
        double truncated = (long)input;
        double remainder = input - truncated;

        if (remainder == 0) {
            return input;
        }

        double magnitude = Math.pow(10,
            cutoff - (int)Math.ceil(
                Math.log10(remainder < 0 ? -remainder : remainder)
            )
        );

        return truncated + (Math.round(remainder * magnitude) / magnitude);
    }
}
