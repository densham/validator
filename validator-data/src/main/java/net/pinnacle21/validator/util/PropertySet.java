/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import net.pinnacle21.validator.api.model.ConvertibleProperty;

import java.util.*;

public class PropertySet<T extends Enum<T> & ConvertibleProperty> {
    protected final Map<T, Object> properties;
    private final Set<T> updatableProperties;

    @SafeVarargs
    protected PropertySet(Class<T> type, T...updatable) {
        this.properties = Collections.synchronizedMap(new EnumMap<>(type));
        this.updatableProperties = updatable.length > 0
            ? EnumSet.copyOf(Arrays.asList(updatable))
            : Collections.emptySet();
    }

    public boolean getBoolean(T property) {
        return this.convert(property)
            .map(v -> v != 0)
            .orElseThrow(() -> unsetException(property));
    }

    public Boolean getBoolean(T property, Boolean defaultValue) {
        return this.convert(property)
            .map(v -> v != 0)
            .orElse(defaultValue);
    }

    public int getInteger(T property) {
        return this.convert(property)
            .orElseThrow(() -> unsetException(property));
    }

    public Integer getInteger(T property, Integer defaultValue) {
        return this.convert(property)
            .orElse(defaultValue);
    }

    public String getString(T property) {
        if (!this.hasProperty(property)) {
            throw unsetException(property);
        }

        return this.properties.get(property).toString();
    }

    public String getString(T property, String defaultValue) {
        Object value = this.properties.getOrDefault(property, defaultValue);

        return value != null
                ? value.toString()
                : null;
    }

    public boolean hasProperty(T property) {
        return this.properties.containsKey(property);
    }

    public void setProperty(T property, Object value) {
        if (this.hasProperty(property) && !this.updatableProperties.contains(property)) {
            throw new IllegalArgumentException(String.format(
                "Cannot reassign the property %s", property.toString()
            ));
        }

        if (value == null || (value instanceof String && ((String)value).isEmpty())) {
            return;
        }

        this.properties.put(property, value);
    }

    private Optional<Integer> convert(T property) {
        if (!property.isConvertible()) {
            throw new IllegalArgumentException(String.format(
                "The property %s cannot be converted to a non-String type", property
            ));
        }

        Object value = this.properties.get(property);
        Integer converted = null;

        if (value instanceof Integer) {
            converted = (Integer)value;
        } else if(value instanceof Boolean) {
            converted = (Boolean)value ? 1 : 0;
        } else if (value != null) {
            // This should be made unnecessary by code elsewhere, but just in case
            converted = Integer.valueOf(value.toString());
        }

        return Optional.ofNullable(converted);
    }

    private static IllegalArgumentException unsetException(Object property) {
        return new IllegalArgumentException(String.format("The property %s is not set", property.toString()));
    }
}
